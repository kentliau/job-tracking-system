### Requirements

- Homestead (recommended), other server stack may require custom configuration.
    - VirtualBox <https://www.virtualbox.org>
    - Vagrant <https://www.vagrantup.com>
    - Homestead <http://laravel.com/docs/4.2/homestead>
- `Composer`. <https://getcomposer.org>
- Homestead come with `NodeJs`, `NPM`, `Bower` and `Gulp` preinstalled. So you can `vagrant ssh` into your Homestead VM and use the tools. But it is recommended to install local globally in your computer, so you can use it local globally.
    - `NodeJs` and `NPM` <http://nodejs.org>
    - `Bower` <http://bower.io>
    - `Gulp` <http://gulpjs.com>

### Up and run development

- `$ git clone <repo-uri>`
- `$ cd job-tracking-project/src/`
- Add your `$ hostname` to job-tracking-project/src/bootstrap/start.php `development` array
- `$ composer install --dev`
- `$ npm install --dev`
- `$ bower install`
- Start Homestead
    - Configure your Homestead.yaml to match your project directory and domain.
    - Configure your `/etc/hosts` file to include your domain. Add this line `192.168.10.10 jts.app`
    - `$ cd <your-homestead.yaml-location>`
    - `$ vagrant up`
- `$ cd job-tracking-project/src/`
- `$ php artisan migrate`
- `$ php artisan db:seed`
- `$ open http://jts.app`

Note 1: Your host machine(machine you use to power your VM) is development environment and Homestead(VM) is local environment.

Note 2: Every time you start Homestead, the database is brand new, it means you have to `migrate` and/or 'seed' again in order to have database deploy correctly. You can `vagrant ssh` to login into your Homestead VM to perform artisan command if you never configure your environment correctly.

Note 3: For updating dependecies, you should use `composer update --dev`, `npm update --dev`, and `bower update` respectively.

Note 4: To upload large files in Nginx (Nginx default it to 1MB per request), add `client_max_body_size 100m;` to your Nginx configuration at Homestead/scripts/serve.sh, you can add it after your Homestead had started, you just need to run `$ vagrant provision`.

```
    .
    .
    .
    access_log off;
    error_log  /var/log/nginx/$1-error.log error;

    error_page 404 /index.php;

    sendfile off;
    client_max_body_size 100m;

    location ~ \.php$ {
    .
    .
    .

```

### Deployment

- Vary on different hosting infrastructure.