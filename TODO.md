- [] Make creation of a shipment as simple as possible, then redirect them to edit, possibly use modal pop up. This is to prevent conflict of file number when multiple users is using the apps by setting the file number at first.

- [x] Purge empty data to null before saving to database
- [x] Sanitize data before saving to database