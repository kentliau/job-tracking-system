// $(function() {

//     $('#side-menu').metisMenu();

// });

// //Loads the correct sidebar on window load,
// //collapses the sidebar on window resize.
// // Sets the min-height of #page-wrapper to window size
// $(function() {
//     $(window).bind("load resize", function() {
//         topOffset = 50;
//         width = (this.window.innerWidth > 0) ? this.window.innerWidth : this.screen.width;
//         if (width < 768) {
//             $('div.navbar-collapse').addClass('collapse')
//             topOffset = 100; // 2-row-menu
//         } else {
//             $('div.navbar-collapse').removeClass('collapse')
//         }

//         height = (this.window.innerHeight > 0) ? this.window.innerHeight : this.screen.height;
//         height = height - topOffset;
//         if (height < 1) height = 1;
//         if (height > topOffset) {
//             $("#page-wrapper").css("min-height", (height) + "px");
//         }
//     })
// })

// browser window scroll (in pixels) after which the "menu" link is shown
var offset = 64;
var navigationContainer = $('.navbar-default.sidebar');

function checkMenu() {
    if( $(window).scrollTop() > offset && !navigationContainer.hasClass('is-fixed')) {
        navigationContainer.addClass('is-fixed');
    } else if ($(window).scrollTop() <= offset) {
        navigationContainer.removeClass('is-fixed');
    }
}

$(window).scroll(function(e){
    checkMenu();
});

$(function(){
    $(".table-column-resizable").resizableColumns({
        store: window.store
    });
});

$(".datetime-control").datetimepicker({
    pickTime: true,
    showToday: true
}).on('dp.change', function(e){
    var $this = $(this);
    var mirrorElement = $($this.data('mirror'));

    mirrorElement.val(e.date.format('YYYY-MM-DD HH:mm:ss'));
});

/**
 * Taken from http://paste.laravel.com/b8n
 *
 * Restfulize any hiperlink that contains a data-method attribute by
 * creating a mini form with the specified method and adding a trigger
 * within the link.
 * Requires jQuery!
 *
 * Ex in Laravel:
 *     <a href="{{ route('resources.destroy', $resource->id) }}" data-method="delete">destroy</a>
 *     // Will trigger the route Route::delete('users')
 *
 */
$(function(){
    $('[data-method]:not(:has(form))').append(function(){
        return "\n"+
        "<form action='"+$(this).attr('href')+"' method='POST' style='display:none'>\n"+
        "   <input type='hidden' name='_method' value='"+$(this).attr('data-method')+"'>\n"+
        "</form>\n"
    })
    .removeAttr('href')
    .attr('onclick','$(this).find("form").submit();');
});

$('.selectize').selectize({
    valueField: 'id',
    labelField: 'name',
    sortField: [{field: 'name', direction: 'asc'}],
    searchField: 'name',
    maxItems: 1,
    create: function(input, callback) {
        var data = this.$input.data('data');
        var fieldName = 'name';
        var response = {};
        $.ajax({
            url: '/' + data,
            //async: false,
            method: 'post',
            data: fieldName + '=' + input,
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function(res) {
                callback(res);
            }
        });
    },
    render: {
        item: function(item, escape) {
          return '<div>' + item.name + '</div>';
        },
        option: function(item, escape) {
            return '<div>' + item.name + '</div>';
        }
    },
    preload: true,
    load: function(query, callback){
        var data = this.$input.data('data');
        $.ajax({
            url: '/' + data,
            method: 'GET',
            success: function(res) {
                callback(res);
            }
        });

    }
});

$('.selectize-multiple').selectize({
    valueField: 'id',
});


