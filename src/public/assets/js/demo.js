
/**
 _____  ______ __  __  ____
 |  __ \|  ____|  \/  |/ __ \
 | |  | | |__  | \  / | |  | |
 | |  | |  __| | |\/| | |  | |
 | |__| | |____| |  | | |__| |
 |_____/|______|_|  |_|\____/
 */

(function(){
    $(function(){
        var processingCount = 12, completedCount = 8,uncompletedCount = 6, cancelledCount = 2;

        setInterval(function(){
            totalIndividualChart.load(
                {
                    columns: [
                        ['Processing', processingCount],
                        ['Completed', completedCount],
                        ['Uncompleted', uncompletedCount],
                        ['Cancelled', cancelledCount]
                    ]
                }
            );
            processingCount += Math.floor(Math.random() * (10 - 5)) + 5;
            completedCount += Math.floor(Math.random() * (20 - 1)) + 1;
            uncompletedCount += Math.floor(Math.random() * (1)) + 0;
            cancelledCount += Math.floor(Math.random() * (2 - 1)) + 1;

            if(processingCount > 100)
            {
                processingCount = 12;
                completedCount = 8;
                uncompletedCount = 6;
                cancelledCount = 2;
            }

        }, 2000);
    });
})();

//demo
(function(){
    $(function(){
        var processingCount = 120, completedCount = 80,uncompletedCount = 60, cancelledCount = 20;

        setInterval(function(){
            totalJobsChart.load(
                {
                    columns: [
                        ['Processing', processingCount],
                        ['Completed', completedCount],
                        ['Uncompleted', uncompletedCount],
                        ['Cancelled', cancelledCount]
                    ]
                }
            );
            processingCount += Math.floor(Math.random() * (20 - 5)) + 5;
            completedCount += Math.floor(Math.random() * (30 - 1)) + 1;
            uncompletedCount += Math.floor(Math.random() * (4)) + 0;
            cancelledCount += Math.floor(Math.random() * (4 - 1)) + 1;

            if(processingCount > 300)
            {
                processingCount = 12;
                completedCount = 8;
                uncompletedCount = 6;
                cancelledCount = 2;
            }

        }, 4000);
    });
})();


(function(){
    $notificationTable = $('#notificationsTable');
    setInterval(function(){
        $notificationTable
            .find('tr').eq(1).toggleClass('bounceIn animated');
    }, 4000)
})();