<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get( '/', [
  'as'   => 'home',
  'uses' => 'HomeController@index'
] );

Route::resource( 'sessions', 'SessionsController' );
Route::get( 'logout', [ 'as' => 'logout', 'uses' => 'SessionsController@destroy' ] );

Route::group( array( 'before' => 'csrf|auth' ), function ()
{

  Route::get( 'shipments/{typeAndYear}/{fileNumber}', 'ShipmentsController@showByFileNumber' )
       ->where( 'typeAndYear', '(im|ex|IM|EX)\-[0-9]{2}' )
       ->where( 'fileNumber', '[0-9]{3}' );

  Route::resource( 'shipments', 'ShipmentsController' );
  Route::resource( 'shipments.items', 'ShipmentsItemsController' );
  Route::resource( 'shipments.tasks', 'ShipmentsTasksController' );
  Route::resource( 'shipments.documents', 'ShipmentsDocumentsController' );
  Route::resource( 'shipments.costsheets', 'ShipmentsCostsheetsController' );
  Route::resource( 'shipments.invoices', 'ShipmentsInvoicesController' );
  Route::resource( 'shipments.invoices.items', 'ShipmentsInvoicesItemsController' );

  Route::resource( 'documents', 'DocumentsController' );
  Route::resource( 'ports', 'PortsController' );
  Route::resource( 'vessels', 'VesselsController' );

  Route::resource( 'customers', 'CustomersController' );


  Route::resource( 'users', 'UsersController' );
  Route::resource( 'groups', 'GroupsController' );

  Route::get( 'dashboard', [ 'as' => 'dashboard', function () { return View::make( 'dashboard.index' ); } ] );

  Route::get( 'hrm', function () { return Redirect::route('users.index'); } );
  Route::get( 'crm', function () { return Redirect::route('customers.index'); } );
  Route::get( 'rm', function () { return Redirect::route('documents.index'); } );

  Route::get( 'search', function () { return Redirect::to( 'shipments/' . Input::get('q') ); } );
  Route::get( 'endpoints', function () { return View::make( 'static.endpoints' ); } );
  Route::get( 'help', function () { return View::make( 'static.help' ); } );

} );


Route::get( 'debug', function ()
{

  $shipmentId = 1;
  $invoiceId  = 89;
  Kint::dump(
    JTS\Entities\Shipments\Shipment::findOrFail( $shipmentId )
                                   ->invoices()->findOrFail( $invoiceId )
                                   ->update( [ ] )
  );
  die();
} );