<?php namespace JTS\Entities\Customers;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Customer extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'CustomerPresenter';

  public static $rules = [
    'name'         => [ 'required' ],
    'contact_name' => '',
    'description'  => '',
    'phone_number' => '',
    'fax_number'   => '',
    'email'        => [ 'email' ],
    'address'      => '',
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  /**
   * Relationships
   *
   */
  public function shipments()
  {
    return $this->hasMany( 'JTS\Entities\Shipments\Shipment', 'shipper_id' );
  }

  public function consigns()
  {
    return $this->hasMany( 'JTS\Entities\Shipments\Shipment', 'consignee_id' );
  }

}
