<?php namespace JTS\Entities\Customers;

use JTS\Core\Repositories\RepositoryTrait;

class DbCustomerRepository implements CustomerRepositoryInterface {

  use RepositoryTrait;


  /**
   * @var \JTS\Entities\Customers\Customer
   */
  private $model;

  function __construct(Customer $customer)
  {
    $this->model = $customer;
  }

  public function getSelectionListing()
  {
    return $this->model->lists( 'name', 'name' );
  }


}