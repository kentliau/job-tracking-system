<?php namespace JTS\Entities\Customers;

use JTS\Core\Presenters\AbstractPresenter;
use JTS\Core\Presenters\PresentableInterface;

class CustomerPresenter extends AbstractPresenter implements PresentableInterface {

  public function name()
  {
    return $this->model->name ? : '';
  }
}
