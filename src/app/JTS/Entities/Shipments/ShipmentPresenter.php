<?php namespace JTS\Entities\Shipments;

use Carbon\Carbon;
use JTS\Core\Presenters\AbstractPresenter;
use JTS\Core\Presenters\PresentableInterface;
use JTS\Entities\Customers\Customer;
use JTS\Entities\Resources\Ports\Port;
use JTS\Entities\Resources\Vessels\Vessel;
use JTS\Entities\Resources\Documents\Document;

class ShipmentPresenter extends AbstractPresenter implements PresentableInterface {

  /**
   * @return string
   */
  public function type()
  {
    return strtoupper( $this->model->type );
  }

  /**
   * @return string
   */
  public function fileNumber()
  {
    $type       = trans( 'jts.' . $this->model->type . '_abbr' );
    $fileNumber = str_pad( $this->model->file_number, 3, '0', STR_PAD_LEFT );
    $year       = Carbon::parse( $this->model->file_date )->format( 'y' );
    $separator  = '/';

    return $type . '-' . $year . $separator . $fileNumber;
  }

  /**
   * @return string
   */
  public function fileDate()
  {
    return Carbon::parse( $this->model->file_date )
                 ->format( \Config::get( 'app.date_format' ) );
  }

  /**
   * @return string
   */
  public function statusClass()
  {
    if ( $this->model->status === 'processing' )
    {
      return 'success';
    }
    elseif ( $this->model->status === 'completed' )
    {
      return 'primary';
    }
    elseif ( $this->model->status === 'uncompleted' )
    {
      return 'danger';
    }
    else
    {
      return 'default';
    }
  }

  /**
   * @return string
   */
  public function description()
  {
    $max_words    = 10;
    $phrase_array = explode( ' ', $this->model->description );
    if ( count( $phrase_array ) > $max_words && $max_words > 0 )
    {
      $phrase = implode( ' ', array_slice( $phrase_array, 0, $max_words ) ) . '...';
    }
    else
    {
      $phrase = $this->model->description;
    }

    return $phrase;
  }

  /**
   * @return Customer
   */
  public function shipper()
  {
    return $this->model->shipper ? : new Customer();
  }

  /**
   * @return Customer
   */
  public function consignee()
  {
    return $this->model->consignee ? : new Customer();
  }

  /**
   * @return Port
   */
  public function loadPort()
  {
    return $this->model->loadPort ? : new Port();
  }

  /**
   * @return Port
   */
  public function dischargePort()
  {
    return $this->model->dischargePort ? : new Port();
  }

  /**
   * @return Vessel
   */
  public function vessel()
  {
    return $this->model->vessel ? : new Vessel();
  }

  /**
   * @return string
   */
  public function vesselLot()
  {
    return strtoupper( $this->model->vessel_lot ) ? : '';
  }

  /**
   * @return string
   */
  public function departureDate()
  {
    if ( $this->model->departure_date )
    {
      return Carbon::parse( $this->model->departure_date )
                   ->format( \Config::get( 'app.date_format' ) );
    }
  }

  /**
   * @return string
   */
  public function estimatedArrivalDate()
  {
    if ( $this->model->estimated_arrival_date )
    {
      return Carbon::parse( $this->model->estimated_arrival_date )
                   ->format( \Config::get( 'app.date_format' ) . ' h:i A' );
    }
    else
    {
      return '';
    }
  }

  /**
   * @return string
   */
  public function arrivedDate()
  {
    if ( $this->model->arrived_date )
    {
      return Carbon::parse( $this->model->arrived_date )
                   ->format( \Config::get( 'app.date_format' ) );
    }
  }

  /**
   * @return string
   */
  public function billOfLadingNumber()
  {
    return $this->model->bill_of_lading_number ? : '';
  }

}
