<?php namespace JTS\Entities\Shipments\Invoices;

use Input;
use JTS\Core\Repositories\RepositoryTrait;
use JTS\Entities\Shipments\Shipment;

class DbInvoiceRepository implements InvoiceRepositoryInterface {

  use RepositoryTrait;

  /**
   * @var Invoice
   */
  private $model;

  function __construct(Invoice $invoice)
  {
    $this->model = $invoice;
  }

  public function getNewDefault()
  {
    $data = [
      'invoice_number' => $this->getNextInvoiceNumber(),
    ];

    $invoice = $this->model->newInstance( $data );

    return $invoice;
  }
  public function getNextInvoiceNumber()
  {
    $max = Invoice::max( 'invoice_number' );

    return ( (int) $max + 1 );
  }

  public function getShipmentInvoices(Shipment $shipment)
  {
    $models = $shipment->invoices()
                       ->orderBy( 'created_at', 'desc' )
                       ->paginate( Input::has( 'limit' ) ? Input::get( 'limit' ) : 10 );

    return $models;
  }

  public function create($shipmentId, array $attributes)
  {
    $attributes[ 'shipment_id' ] = $shipmentId;

    return $this->model->create( $attributes );
  }

}