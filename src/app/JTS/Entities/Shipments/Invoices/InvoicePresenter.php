<?php namespace JTS\Entities\Shipments\Invoices;

use JTS\Core\Presenters\AbstractPresenter;
use JTS\Core\Presenters\PresentableInterface;

class InvoicePresenter extends AbstractPresenter implements PresentableInterface {

  /**
   * @return string
   */
  public function invoiceNumber()
  {
    return str_pad( $this->model->invoice_number, 6, '0', STR_PAD_LEFT );
  }

}
