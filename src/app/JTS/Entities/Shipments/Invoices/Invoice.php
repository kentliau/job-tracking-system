<?php namespace JTS\Entities\Shipments\Invoices;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Invoice extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'InvoicePresenter';

  protected $table = 'shipments_invoices';

  public static $rules = [
    'shipment_id'    => [ 'exists:shipments,id' ],
    'invoice_number' => [ 'unique:shipments_invoices' ],
    'description'    => '',
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  public function setItemsAttribute($value)
  {
    // $items = ShipmentInvoiceItem::create($value);
  }

  /**
   * Relationships
   *
   */
  public function shipment()
  {
    return $this->belongsTo( 'JTS\Entities\Shipments\Shipment' );
  }

  public function items()
  {
    return $this->hasMany( 'JTS\Entities\Shipments\Invoices\Item' );
  }

}
