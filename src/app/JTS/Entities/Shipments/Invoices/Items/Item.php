<?php namespace JTS\Entities\Shipments\Invoices\Items;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Item extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'ItemPresenter';

  protected $table = 'shipments_invoices_items';

  public static $rules = [
    'invoice_id'  => [ 'required', 'exists:shipments_invoices,id' ],
    'description' => [ 'required' ],
    'currency'    => [ 'required' ],
    'amount'      => [ 'numeric' ],
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  public function invoice()
  {
    return $this->belongsTo( 'JTS\Entities\Shipments\Invoices\Invoice', 'invoice_id' );
  }

  /**
   * Relationships
   *
   */

}
