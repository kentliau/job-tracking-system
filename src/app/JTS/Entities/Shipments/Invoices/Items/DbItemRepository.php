<?php namespace JTS\Entities\Shipments\Invoices\Items;

use Input;
use JTS\Core\Repositories\RepositoryTrait;
use JTS\Entities\Shipments\Invoices\Invoice;

class DbItemRepository implements ItemRepositoryInterface {

  use RepositoryTrait;

  /**
   * @var Item
   */
  private $model;

  function __construct(Item $item)
  {
    $this->model = $item;
  }

  public function getInvoiceItems(Invoice $invoice)
  {
    $models = $invoice->items()
                      ->orderBy( 'created_at', 'desc' )
                      ->paginate( Input::has( 'limit' ) ? Input::get( 'limit' ) : 10 );

    return $models;
  }

  public function create($invoiceId, array $attributes)
  {
    $attributes[ 'invoice_id' ] = $invoiceId;

    return $this->model->create( $attributes );
  }

}