<?php namespace JTS\Entities\Shipments;

use Exception;
use Carbon\Carbon;
use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;
use JTS\Entities\Customers\Customer;
use JTS\Entities\Resources\Ports\Port;
use JTS\Entities\Resources\Vessels\Vessel;

class Shipment extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'ShipmentPresenter';

  public static $rules = [
    'file_number'            => [ 'required', 'numeric', 'unique_with:shipments,type,file_year' ],
    'file_year'              => [ 'digits:4' ],
    'file_date'              => [ 'required', 'date_format:Y-m-d H:i:s' ],
    'type'                   => [ 'required', 'in:import,export' ],
    'status'                 => [ 'in:processing,uncompleted,completed,cancelled' ],
    'priority'               => [ 'in:low,standard,high' ],
    'shipper_id'             => [ 'numeric', 'exists:customers,id' ],
    'consignee_id'           => [ 'numeric', 'exists:customers,id' ],
    'description'            => '',
    'load_port_id'           => [ 'numeric', 'exists:ports,id' ],
    'discharge_port_id'      => [ 'numeric', 'exists:ports,id' ],
    'vessel_id'              => [ 'numeric', 'exists:vessels,id' ],
    'vessel_lot'             => '',
    'departure_date'         => 'date_format:Y-m-d H:i:s',
    'estimated_arrival_date' => 'date_format:Y-m-d H:i:s',
    'arrived_date'           => 'date_format:Y-m-d H:i:s',
    'bill_of_lading_number'  => '',
    'expired_at'             => [ 'date_format:Y-m-d H:i:s' ],
  ];

  protected $fillable = [
    'file_number',
    'file_date',
    'type',
    'status',
    'priority',
    'shipper_id',
    'consignee_id',
    'description',
    'load_port_id',
    'discharge_port_id',
    'vessel_id',
    'vessel_lot',
    'departure_date',
    'estimated_arrival_date',
    'arrived_date',
    'bill_of_lading_number',
  ];

  protected $guarded = [ 'id', 'file_year', 'expired_at' ];

  public function getDates()
  {
    return [
      'file_date',
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  /**
   * To check whether a shipment is editable or expired
   *
   * @return bool
   */
  public function isExpired()
  {
    $now       = Carbon::now();
    $expiredAt = $this->expired_at;
    $seconds   = $now->diffInSeconds( $expiredAt, false );

    if ( $seconds <= 0 )
    {
      return true;
    }

    return false;
  }

  /**
   * We don't really want to let anyone set file_year,
   * file_year exists because of we want database build in constraint.
   * So file_year is auto set with file_date by grabbing its year
   *
   * @param
   */
  public function setFileDateAttribute($value)
  {
    $this->attributes[ 'file_date' ] = $value;
    $year                            = Carbon::parse( $value )->format( 'Y' );
    $this->attributes[ 'file_year' ] = $year;
  }

  /**
   * Relationship
   *
   */
  public function items()
  {
    return $this->hasMany( 'JTS\Entities\Shipments\Items\Item' );
  }

  public function documents()
  {
    return $this->hasMany( 'JTS\Entities\Shipments\Documents\Document' );
  }

  public function invoices()
  {
    return $this->hasMany( 'JTS\Entities\Shipments\Invoices\Invoice' );
  }

  public function shipper()
  {
    return $this->belongsTo( 'JTS\Entities\Customers\Customer', 'shipper_id' );
  }

  public function consignee()
  {
    return $this->belongsTo( 'JTS\Entities\Customers\Customer', 'consignee_id' );
  }

  public function loadPort()
  {
    return $this->belongsTo( 'JTS\Entities\Resources\Ports\Port', 'load_port_id' );
  }

  public function dischargePort()
  {
    return $this->belongsTo( 'JTS\Entities\Resources\Ports\Port', 'discharge_port_id' );
  }

  public function vessel()
  {
    return $this->belongsTo( 'JTS\Entities\Resources\Vessels\Vessel' );
  }

  public function users()
  {
    return $this->belongsToMany( 'JTS\Entities\Users\User' );
  }

}
