<?php namespace JTS\Entities\Shipments\Items;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Item extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'ItemPresenter';

  protected $table = 'shipments_items';

  public static $rules = [
    'shipment_id'   => [ 'required', 'exists:shipments,id' ],
    'name'          => [ 'required' ],
    'serial_number' => '',
    'description'   => '',
    'category'      => '',
    'size'          => '',
    'value'         => '',
    'quantity'      => '',
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  public function shipment()
  {
    return $this->belongsTo( 'JTS\Entities\Shipments\Shipment' );
  }

  /**
   * Relationships
   *
   */

}
