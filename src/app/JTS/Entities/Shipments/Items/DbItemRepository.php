<?php namespace JTS\Entities\Shipments\Items;

use Input;
use JTS\Core\Repositories\RepositoryTrait;
use JTS\Entities\Shipments\Shipment;

class DbItemRepository implements ItemRepositoryInterface {

  use RepositoryTrait;

  /**
   * @var \JTS\Entities\Shipments\Items\Item
   */
  private $model;

  function __construct(Item $item)
  {
    $this->model = $item;
  }

  /**
   * @param \JTS\Entities\Shipments\Shipment $shipment
   *
   * @return mixed
   */
  public function getAllOfShipment(Shipment $shipment)
  {
    $models = $shipment->items()
                       ->orderBy( 'created_at', 'desc' )
                       ->paginate( Input::has( 'limit' ) ? Input::get( 'limit' ) : 10 );

    return $models;
  }

  /**
   * @param \JTS\Entities\Shipments\Shipment $shipment
   * @param int                              $itemId
   *
   * @return mixed
   */
  public function getByIdOfShipment(Shipment $shipment, $itemId)
  {
    $model = $shipment->items()->findOrFail( $itemId );

    return $model;
  }

  public function create($shipmentId, array $attributes)
  {
    $attributes[ 'shipment_id' ] = $shipmentId;

    return $this->model->create( $attributes );
  }

  /**
   * @param \JTS\Entities\Shipments\Shipment $shipment
   * @param array                            $attributes
   */
  public function createForShipment(Shipment $shipment, array $attributes)
  {

  }

}