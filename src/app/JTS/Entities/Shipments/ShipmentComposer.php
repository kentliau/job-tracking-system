<?php namespace JTS\Entities\Shipments;

class ShipmentComposer {

  /**
   * @var ShipmentRepositoryInterface
   */
  private $model;

  function __construct(ShipmentRepositoryInterface $shipment)
  {
    $this->model = $shipment;
  }

  public function compose($view)
  {
    $shipmentId = isset( $view->getData()[ 'shipmentId' ] ) ? $view->getData()[ 'shipmentId' ] : $view->getData()[ 'shipment' ][ 'id' ];
    $fileNumber = $this->model->getById( $shipmentId )->present()->fileNumber;
    $view->with( 'fileNumber', $fileNumber );
  }

}
