<?php namespace JTS\Entities\Shipments\Documents;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Document extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'DocumentPresenter';

  protected $table = 'shipments_documents';

  public static $rules = [
    'shipment_id'   => [ 'exists:shipments,id' ],
    'document_id'   => [ 'exists:documents,id' ],
    'name'          => '',
    'serial_number' => '',
  ];

  protected $guarded = [ 'id' ];

  protected $fillable = [ 'shipment_id',
                          'document_id',
                          'name',
                          'serial_number' ];

  /**
   * Relationships
   *
   */
  public function shipment()
  {
    return $this->belongsTo( 'JTS\Entities\Shipments\Shipment' );
  }

  public function document()
  {
    return $this->belongsTo( 'JTS\Entities\Resources\Documents\Document' );
  }

}
