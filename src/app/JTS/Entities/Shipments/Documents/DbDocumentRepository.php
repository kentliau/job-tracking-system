<?php namespace JTS\Entities\Shipments\Documents;

use Input;
use JTS\Core\Repositories\RepositoryTrait;
use JTS\Entities\Shipments\Shipment;

class DbDocumentRepository implements DocumentRepositoryInterface {

  use RepositoryTrait;

  /**
   * @var Document
   */
  private $model;

  function __construct(Document $document)
  {
    $this->model = $document;
  }

  public function getShipmentDocuments(Shipment $shipment)
  {
    $models = $shipment->documents()
                       ->orderBy( 'created_at', 'desc' )
                       ->paginate( Input::has( 'limit' ) ? Input::get( 'limit' ) : 10 );

    return $models;
  }

  public function create($shipmentId, array $attributes)
  {
    $attributes[ 'shipment_id' ] = $shipmentId;
    $model                       = $this->model->create( $attributes );

    $documentId = $model->id;

    $this->putFilesToStorage( $shipmentId, $documentId );

    return $model;
  }

  public function update($id, array $attributes)
  {
    $this->model->findOrFail( $id )->update( $attributes );
    $model = $this->model->findOrFail( $id );

    $shipmentId = $model[ 'shipment_id' ];
    $documentId = $model[ 'id' ];

    $this->putFilesToStorage( $shipmentId, $documentId );

    return $model;
  }

  /**
   * @param $shipmentId
   * @param $documentId
   */
  private function putFilesToStorage($shipmentId, $documentId)
  {
    if ( Input::hasFile( 'files' ) )
    {
      foreach ( Input::file( 'files' ) as $index => $file )
      {
        $ext  = $file->getClientOriginalExtension();
        $name = $file->getClientOriginalName();
        $file->move( storage_path() . "/jts/shipments/{$shipmentId}/documents/{$documentId}/", "{$name}" );
      }
    }
  }

}