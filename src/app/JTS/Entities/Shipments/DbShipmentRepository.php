<?php namespace JTS\Entities\Shipments;

use JTS\App as JTS;
use Input;
use Carbon\Carbon;
use JTS\Core\Repositories\RepositoryTrait;

class DbShipmentRepository implements ShipmentRepositoryInterface {


  use RepositoryTrait;

  /**
   * @var \JTS\Entities\Shipments\Shipment
   */
  protected $model;

  protected $relations = ['shipper', 'consignee', 'loadPort', 'dischargePort', 'vessel', 'items', 'documents', 'invoices', 'users' ];

  /**
   * @param \JTS\Entities\Shipments\Shipment $shipment
   */
  function __construct(Shipment $shipment)
  {
    $this->model = $shipment;
  }

  public function getNewDefault()
  {
    $type = Input::get( 'type' );

    $data = [
      'file_number' => $this->getNextFileNumber( $type ? : JTS::$defaultType ),
      'file_date'   => Carbon::now(),
      'type'        => $type ? : JTS::$defaultType,
      'status'      => JTS::$defaultStatus,
      'priority'    => JTS::$defaultPriority,
    ];

    $shipment = $this->model->newInstance( $data );

    return $shipment;
  }

  /**
   * Get next file_number base on 2 constraint,
   * the type (export|import),
   * the year, and
   * return the next largest file_number.
   *
   * @param string $type Possible value (import|export)
   * @param int    $year Four digits year, default to current year.
   *
   * @return int
   */
  public function getNextFileNumber($type, $year = null)
  {
    if ( is_null( $year ) )
    {
      $year = Carbon::now()->year;
    }

    $max = $this->model->withTrashed()
                       ->where( 'file_year', '=', $year )
                       ->where( 'type', '=', $type )
                       ->max( 'file_number' );

    return ( (int) $max + 1 );
  }

  public function getAll()
  {
    $type      = Input::has( 'type' ) ? [ Input::get( 'type' ) ] : JTS::$type;
    $status    = Input::has( 'status' ) ? [ Input::get( 'status' ) ] : JTS::$status;
    $offset    = Input::get( 'offset' ) ? : 0;
    $limit     = Input::get( 'limit' ) ? : 10;
    $direction = Input::get( 'direction' ) ? : 'desc';

    $shipments = $this->model->with( $this->relations )
                             ->whereIn( 'type', $type )
                             ->whereIn( 'status', $status )
                             ->orderBy( 'file_date', $direction )
                             ->orderBy( 'file_number', $direction )
                             ->paginate( $limit );

    return $shipments;
  }

  public function getById($id)
  {
    return
      $this->model->with( $this->relations )
                  ->findOrFail( $id );
  }

  public function getByFileNumber($type, $year, $fileNumber)
  {
    $shipment = $this->model->with( $this->relations )
                            ->where( 'type', '=', $type )
                            ->where( 'file_year', '=', $year )
                            ->where( 'file_number', '=', $fileNumber )
                            ->firstOrFail();

    return $shipment;
  }

  public function search($query)
  {
    // customer
    // description
  }

  public function items()
  {
    // return ItemRepositoryInterface
    // $this->shipment->getById(1)->
  }

  public function documents()
  {
    // return DocumentRepositoryInterface
  }

  public function invoices()
  {
    // return InvoiceRepositoryInterface
  }

}
