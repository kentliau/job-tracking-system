<?php namespace JTS\Entities\Users;

use JTS\Core\Entities\AbstractEntity;
use Cartalyst\Sentry\Users\Eloquent\User as SentryUser;
use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;
use JTS\Core\Presenters\PresentableTrait;

class User extends SentryUser implements UserInterface, RemindableInterface {

  use UserTrait, RemindableTrait, PresentableTrait;

  protected $presenter = 'UserPresenter';

  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'users';

  /**
   * The attributes excluded from the model's JSON form.
   *
   * @var array
   */
  protected $hidden = [ 'password', 'remember_token' ];

  public static $rules = [ ];

  protected $fillable = [];

  protected $guarded = [ 'id', 'group_id' ];

  /**
   * Relationships
   *
   */
  public function shipments()
  {
    return $this->belongsToMany( 'JTS\Entities\Shipments\Shipment' );
  }

}
