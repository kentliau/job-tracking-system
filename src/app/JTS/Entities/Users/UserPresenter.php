<?php namespace JTS\Entities\Users;

use JTS\Core\Presenters\AbstractPresenter;
use JTS\Core\Presenters\PresentableInterface;

class UserPresenter extends AbstractPresenter implements PresentableInterface {

  public function identifier()
  {
    if ( ! empty( $this->model->first_name ) )
    {
      return $this->model->first_name . ' ' . $this->model->last_name;
    }
    if ( ! empty( $this->model->email ) )
    {
      return $this->model->email;
    }

    return $this->model->id;
  }

  public function group()
  {
    if ( $this->model->hasAccess( 'superuser' ) )
    {
      return 'Superuser';
    };

    return $this->model->groups()->first()[ 'name' ];
  }

}
