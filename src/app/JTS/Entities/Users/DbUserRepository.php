<?php namespace JTS\Entities\Users;

use JTS\Core\Repositories\RepositoryTrait;

class DbUserRepository implements UserRepositoryInterface {

  use RepositoryTrait;


  /**
   * @var \JTS\Entities\Users\User
   */
  private $model;

  function __construct(User $user)
  {
    $this->model = $user;
  }

  public function getSelectionListing()
  {
    $arr = $this->model->lists( 'email', 'id' );

    return $arr;
  }



}