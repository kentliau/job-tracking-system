<?php namespace JTS\Entities\Users\Groups;

use JTS\Core\Repositories\RepositoryTrait;

class DbGroupRepository implements GroupRepositoryInterface {

  use RepositoryTrait;


  /**
   * @var \JTS\Entities\Users\Groups\Group
   */
  private $model;

  function __construct(Group $group)
  {
    $this->model = $group;
  }

  public function getSelectionListing()
  {
    $arr = $this->model->lists( 'name', 'id' );

    return $arr;
  }

}