<?php namespace JTS\Entities\Users;

class UserComposer {

  private $model;

  function __construct(UserRepositoryInterface $user)
  {
    $this->model = $user;
  }

  public function compose($view)
  {

    $userSelectionListing = $this->model->getAll();

    $view->with( 'userSelectionListing', $userSelectionListing );
  }

}
