<?php namespace JTS\Entities\Resources\Ports;

use JTS\Core\Repositories\RepositoryTrait;

class DbPortRepository implements PortRepositoryInterface {

  use RepositoryTrait;


  /**
   * @var \JTS\Entities\Resources\Ports\Port
   */
  private $model;

  function __construct(Port $port)
  {
    $this->model = $port;
  }

  public function getSelectionListing()
  {
    $arr = $this->model->lists( 'name', 'name' );

    return $arr;
  }

}