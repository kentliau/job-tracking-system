<?php namespace JTS\Entities\Resources\Ports;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Port extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'PortPresenter';

  public static $rules = [
    'name'         => [ 'required' ],
    'code'         => '',
    'description'  => '',
    'contact_name' => '',
    'phone_number' => '',
    'fax_number'   => '',
    'email'        => [ 'email' ],
    'address'      => '',
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  /**
   * Relationships
   *
   */

}
