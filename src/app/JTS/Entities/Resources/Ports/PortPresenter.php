<?php namespace JTS\Entities\Resources\Ports;

use JTS\Core\Presenters\AbstractPresenter;
use JTS\Core\Presenters\PresentableInterface;

class PortPresenter extends AbstractPresenter implements PresentableInterface {

  /**
   * @return string
   */
  public function name()
  {
    return $this->model->name ? : '';
  }

}
