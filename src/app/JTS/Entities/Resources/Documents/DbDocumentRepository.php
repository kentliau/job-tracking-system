<?php namespace JTS\Entities\Resources\Documents;

use Input;
use JTS\Core\Repositories\RepositoryTrait;

class DbDocumentRepository implements DocumentRepositoryInterface {

  use RepositoryTrait;

  /**
   * @var Document
   */
  private $model;

  function __construct(Document $document)
  {
    $this->model = $document;
  }

  public function getSelectionListing()
  {
    return $this->model->lists( 'name', 'id' );
  }

}