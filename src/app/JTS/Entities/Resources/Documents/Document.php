<?php namespace JTS\Entities\Resources\Documents;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Document extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'DocumentPresenter';

  public static $rules = [
    'name'        => [ 'required' ],
    'description' => '',
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  /**
   * Relationships
   *
   */

}
