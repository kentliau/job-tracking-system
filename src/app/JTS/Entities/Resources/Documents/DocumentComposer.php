<?php namespace JTS\Entities\Resources\Documents;

class DocumentComposer {

  /**
   * @var DocumentRepositoryInterface
   */
  private $model;

  function __construct(DocumentRepositoryInterface $document)
  {
    $this->model = $document;
  }

  public function compose($view)
  {
    $documentList = $this->model->getSelectionListing();
    $view->with( 'documentListing', $documentList );
  }

}
