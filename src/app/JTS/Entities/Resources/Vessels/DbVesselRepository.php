<?php namespace JTS\Entities\Resources\Vessels;

use JTS\Core\Repositories\RepositoryTrait;

class DbVesselRepository implements VesselRepositoryInterface {

  use RepositoryTrait;


  /**
   * @var \JTS\Entities\Resources\Vessels\Vessel
   */
  private $model;

  function __construct(Vessel $vessel)
  {
    $this->model = $vessel;
  }

  public function getSelectionListing()
  {
    return $this->model->lists( 'name', 'name' );
  }
}