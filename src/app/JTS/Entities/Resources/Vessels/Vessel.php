<?php namespace JTS\Entities\Resources\Vessels;

use JTS\Core\Entities\AbstractEntity;
use JTS\Core\Presenters\PresentableTrait;

class Vessel extends AbstractEntity {

  use PresentableTrait;

  protected $presenter = 'VesselPresenter';

  public static $rules = [
    'name'        => [ 'required' ],
    'code'        => '',
    'description' => '',
  ];

  protected $fillable = [ ];

  protected $guarded = [ 'id' ];

  /**
   * Relationships
   *
   */

}
