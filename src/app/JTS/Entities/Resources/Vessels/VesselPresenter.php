<?php namespace JTS\Entities\Resources\Vessels;

use JTS\Core\Presenters\AbstractPresenter;
use JTS\Core\Presenters\PresentableInterface;

class VesselPresenter extends AbstractPresenter implements PresentableInterface {

  /**
   * @return string
   */
  public function name()
  {
    return ucwords( $this->model->name ) ? : trans( 'ui.missing_field' );
  }

}
