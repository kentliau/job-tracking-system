<?php namespace JTS;

class App {

  /**
   * Enumerations
   *
   */
  public static $type = [ 'export', 'import' ];

  public static $defaultType = 'export';

  public static $status = [ 'processing', 'completed', 'uncompleted', 'cancelled' ];

  public static $defaultStatus = 'processing';

  public static $priority = [ 'low', 'standard', 'high' ];

  public static $defaultPriority = 'standard';

  /**
   * For UI input elements selections
   *
   */
  public static function typeSelection()
  {
    return [
      'export' => trans( 'jts.export' ),
      'import' => trans( 'jts.import' )
    ];
  }

  public static function prioritySelection()
  {
    return [
      'low'      => trans( 'jts.low' ),
      'standard' => trans( 'jts.standard' ),
      'high'     => trans( 'jts.high' ),
    ];
  }

  public static function statusSelection()
  {
    return [
      'processing'  => trans( 'jts.processing' ),
      'completed'   => trans( 'jts.completed' ),
      'uncompleted' => trans( 'jts.uncompleted' ),
      'cancelled'   => trans( 'jts.cancelled' ),
    ];
  }
}