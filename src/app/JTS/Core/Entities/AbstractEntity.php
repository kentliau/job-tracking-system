<?php namespace JTS\Core\Entities;

use Input, Eloquent, Validator;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use JTS\Core\Exceptions\ValidationException;

abstract class AbstractEntity extends Eloquent {

  use SoftDeletingTrait;

  /**
   * Validation rules.
   *
   * @var mixed
   */
  public static $rules = [ ];

  /**
   * Mass assignment permitted column.
   *
   * @var array
   */
  protected $fillable = [ ];

  /**
   * Mass assignment prohibited column.
   *
   * @var array
   */
  protected $guarded = [ ];


  /**
   * Trim attributes
   *
   * @var bool
   */
  protected $trimAttributes = true;

  /**
   * Purge empty attributes to null instead of any other empty value
   *
   * @var bool
   */
  protected $purgeEmptyAttributes = true;

  /**
   * Sanitize illegal HTML characters before storing to database
   *
   * @var bool
   */
  protected $sanitizeAttributes = true;

  /**
   * Define which column should return a Carbon\Carbon instances
   *
   * @return array
   */
  public function getDates()
  {
    return [
      'created_at',
      'updated_at',
      'deleted_at',
    ];
  }

  public function scopeSortable($query)
  {
    $sortBy = Input::get( 'order_by' );
    $asc    = Input::get( 'asc' );

    return $query->orderBy( $sortBy, empty( $asc ) ? 'desc' : 'asc' );
  }

  public function scopePaginable($query)
  {
    return $query->paginate( Input::has( 'limit' ) ? Input::get( 'limit' ) : 10 );
  }


  /**
   * Save with validation
   *
   * @see Illumunate\Database\Eloquent\Model
   */
  public function save(array $options = [ ])
  {
    // If creating a new model we validate at here with standard rules
    if ( ! $this->exists )
    {
      $this->validate( $this->getAttributes(), static::$rules );
    }

    // Here we will try to purge empty attributes before save
    if ( $this->purgeEmptyAttributes )
    {
      $this->purgeEmptyAttributes();
    }

    // Here we will try to trim attributes before save
    if ( $this->trimAttributes )
    {
      $this->trimAttributes();
    }

    // Here we will try to sanitize the attributes before save
    if ( $this->sanitizeAttributes )
    {
      $this->sanitizeAttributes();
    }

    return parent::save( $options );
  }

  /**
   * Update with validation but ignore itself for unique check
   *
   * @see Illumunate\Database\Eloquent\Model
   */
  public function update(array $attributes = [ ])
  {
    $attributes = array_merge( $this->getAttributes(), $attributes );
    $rules      = $this->buildUniqueExclusionRules( $this->id );

    $this->validate( $attributes, $rules );

    return parent::update( $attributes );
  }

  /**
   * Validate data with rules
   *
   * @throws \JTS\Core\Exceptions\ValidationException
   */
  protected static function validate($attributes, $rules)
  {
    $validator = Validator::make( $attributes, $rules );

    if ( $validator->fails() )
    {
      throw new ValidationException( $validator->getMessageBag(), 400, NULL, $validator );
    }
  }

  /**
   * When given an ID and a Laravel validation rules array, this function
   * appends the ID to the 'unique' rules given. The resulting array can
   * then be fed to a Ardent save so that unchanged values
   * don't flag a validation issue. Rules can be in either strings
   * with pipes or arrays, but the returned rules are in arrays.
   *
   * @param int    $id
   * @param string $uniqueColumnName
   *
   * @return array Rules with exclusions applied
   */
  protected static function buildUniqueExclusionRules($uniqueId, $uniqueIdColumnName = 'id')
  {
    $rules = static::$rules;

    foreach ( $rules as $field => &$ruleset )
    {
      // If $ruleset is a pipe-separated string, switch it to array
      $ruleset = ( is_string( $ruleset ) ) ? explode( '|', $ruleset ) : $ruleset;

      foreach ( $ruleset as &$rule )
      {
        if ( strpos( $rule, 'unique_with' ) === 0 )
        {
          $params    = explode( ',', $rule );
          $params[ ] = $uniqueId . ' = ' . $uniqueIdColumnName;
          $rule      = implode( ',', $params );
        }
        elseif ( strpos( $rule, 'unique' ) === 0 )
        {
          $params = explode( ',', $rule );

          // Append field name if needed
          if ( count( $params ) == 1 )
          {
            $params[ 1 ] = $field;
          }

          // if the 3rd param was set, do not overwrite it
          if ( ! is_numeric( @$params[ 2 ] ) )
          {
            $params[ 2 ] = $uniqueId;
          }

          $rule = implode( ',', $params );
        }
      }
    }

    return $rules;
  }

  protected function trimAttributes()
  {
    $attributes = $this->attributesToArray();

    array_walk_recursive( $attributes, function (&$value)
    {
      if ( $value )
      {
        $value = trim( $value );
      }
    } );

    $this->attributes = $attributes;
  }

  /**
   * Purge any empty attributes value to null recursively
   *
   */
  protected function purgeEmptyAttributes()
  {
    $attributes = $this->attributesToArray();

    array_walk_recursive( $attributes, function (&$value)
    {
      if ( trim( $value ) === '' )
      {
        $value = null;
      }
    } );

    $this->attributes = $attributes;
  }

  /**
   * Sanitize all the attributes before we store to database
   *
   */
  protected function sanitizeAttributes()
  {
    $attributes = $this->attributesToArray();

    array_walk_recursive( $attributes, function (&$value)
    {
      if ( $value )
      {
        $value = e( $value );
      }
    } );

    $this->attributes = $attributes;
  }

}