<?php namespace JTS\Core\Validators;

use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider {

  public function register() { }

  public function boot()
  {
    // Registering the validator extension with the validator factory
    $this->app[ 'validator' ]->resolver( function ($translator, $data, $rules, $messages)
    {
      return new UniqueWithValidator(
        $translator,
        $data,
        $rules,
        $messages
      );
    } );
  }

}
