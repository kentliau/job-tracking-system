<?php namespace JTS\Core\Composers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider {

  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register()
  {
    $this->app[ 'view' ]->composer( [
                                      'shipments.edit',
                                      'shipments.items.*',
                                      'shipments.tasks.*',
                                      'shipments.documents.*',
                                      'shipments.costsheets.*',
                                      'shipments.invoices.*',
                                      'shipments.invoices.items.*',
                                    ],
                                    'JTS\Entities\Shipments\ShipmentComposer' );

    $this->app[ 'view' ]->composer( [
                                      'shipments.documents.*',
                                    ],
                                    'JTS\Entities\Resources\Documents\DocumentComposer' );

    $this->app[ 'view' ]->composer( [
                                      'shipments.*',
                                    ],
                                    'JTS\Entities\Users\UserComposer' );
  }


}