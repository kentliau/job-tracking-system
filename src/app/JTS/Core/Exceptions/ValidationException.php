<?php namespace JTS\Core\Exceptions;

use Illuminate\Validation\Validator;

class ValidationException extends \Exception {

  protected $validator;

  public function __construct($message = "", $code = 400, Exception $previous = null, Validator $validator = null)
  {
    $this->validator = $validator;
    parent::__construct( $message, $code, $previous );
  }

  public function getValidator()
  {
    return $this->validator;
  }

}