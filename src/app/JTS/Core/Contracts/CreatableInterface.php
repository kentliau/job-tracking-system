<?php namespace JTS\Core\Contracts;

interface CreatableInterface {

  public function create(array $attributes);

}