<?php namespace JTS\Core\Contracts;

interface SearchableInterface {

  public function search($query);

}