<?php

namespace JTS\Core\Contracts;


interface PaginableInterface {

  public function paginate();

}