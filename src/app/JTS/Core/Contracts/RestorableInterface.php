<?php namespace JTS\Core\Contracts;

interface RestorableInterface {

  public function restore($id);

}