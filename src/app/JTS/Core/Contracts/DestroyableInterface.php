<?php namespace JTS\Core\Contracts;

interface DestroyableInterface {

  public function destroy($id);

}