<?php namespace JTS\Core\Contracts;

interface UpdatableInterface {

  public function update($id, array $attributes);

}