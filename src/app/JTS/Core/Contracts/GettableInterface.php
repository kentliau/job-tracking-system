<?php namespace JTS\Core\Contracts;

interface GettableInterface {

  public function getAll();

  public function getById($id);

}