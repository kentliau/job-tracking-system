<?php namespace JTS\Core\Presenters;

abstract class AbstractPresenter implements \ArrayAccess {

  /**
   * @var
   */
  protected $model;

  /**
   * @param $resource
   */
  public function __construct($resource)
  {
    $this->model = $resource;
  }

  /**
   * Inject the resource to be presented
   *
   * @param mixed
   */
  public function set($resource)
  {
    $this->model = $resource;
  }

  /**
   * Check to see if there is a presenter
   * method. If not pass to the object
   *
   * @param string $name
   *
   * @return string
   */
  public function __get($name)
  {
    if ( method_exists( $this, $name ) )
    {
      return ! is_null( $this->{$name}() ) ? $this->{$name}() : '';
    }

    if ( $this->model->{$name} )
    {
      return $this->model->{$name};
    }
    else
    {
      return '';
    }
  }

  /**
   * @param mixed $offset
   *
   * @return bool
   */
  public function offsetExists($offset)
  {
    return isset( $this->collection[ $offset ] );
  }

  /**
   * @param mixed $offset
   *
   * @return null
   */
  public function offsetGet($offset)
  {
    return isset( $this->collection[ $offset ] ) ? $this->collection[ $offset ] : null;
  }

  /**
   * @param mixed $offset
   * @param mixed $value
   */
  public function offsetSet($offset, $value)
  {
    $this->collection[ $offset ] = $value;
  }

  /**
   * @param mixed $offset
   */
  public function offsetUnset($offset)
  {
    unset( $this->collection[ $offset ] );
  }

}