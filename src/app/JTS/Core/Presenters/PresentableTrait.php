<?php namespace JTS\Core\Presenters;

use JTS\Core\Exceptions\PresenterException;

trait PresentableTrait {

  protected $presenterInstance;

  public function present()
  {

    if ( ! $this->presenter )
    {
      throw new PresenterException( 'Please set the protected $presenter to your presenter path.' );
    }

    if ( ! class_exists( $this->presenter ) )
    {
      $reflector = new \ReflectionClass( $this );
      $namespace = $reflector->getNamespaceName();

      if ( class_exists( $namespace . '\\' . $this->presenter ) )
      {
        $this->presenter = $namespace . '\\' . $this->presenter;
      }
      else
      {
        throw new PresenterException( 'Class not found, please use a qualified class name for protected $presenter property.' );
      }
    }

    // Singleton for that instance
    // therefore you don't make a new presenter instance in the same model instance
    if ( ! isset( $this->presenterInstance ) )
    {
      $this->presenterInstance = new $this->presenter( $this );
    }

    return $this->presenterInstance;
  }

}