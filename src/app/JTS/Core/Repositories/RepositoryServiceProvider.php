<?php namespace JTS\Core\Repositories;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider {

  public function register()
  {
    // Shipments
    $this->app->bind( 'JTS\Entities\Shipments\ShipmentRepositoryInterface',
                      'JTS\Entities\Shipments\DbShipmentRepository' );

    $this->app->bind( 'JTS\Entities\Shipments\Items\ItemRepositoryInterface',
                      'JTS\Entities\Shipments\Items\DbItemRepository' );

    $this->app->bind( 'JTS\Entities\Shipments\Documents\DocumentRepositoryInterface',
                      'JTS\Entities\Shipments\Documents\DbDocumentRepository' );

    $this->app->bind( 'JTS\Entities\Shipments\Invoices\InvoiceRepositoryInterface',
                      'JTS\Entities\Shipments\Invoices\DbInvoiceRepository' );

    $this->app->bind( 'JTS\Entities\Shipments\Invoices\Items\ItemRepositoryInterface',
                      'JTS\Entities\Shipments\Invoices\Items\DbItemRepository' );

    // Customers
    $this->app->bind( 'JTS\Entities\Customers\CustomerRepositoryInterface',
                      'JTS\Entities\Customers\DbCustomerRepository' );

    // Resources
    $this->app->bind( 'JTS\Entities\Resources\Ports\PortRepositoryInterface',
                      'JTS\Entities\Resources\Ports\DbPortRepository' );

    $this->app->bind( 'JTS\Entities\Resources\Vessels\VesselRepositoryInterface',
                      'JTS\Entities\Resources\Vessels\DbVesselRepository' );

    $this->app->bind( 'JTS\Entities\Resources\Documents\DocumentRepositoryInterface',
                      'JTS\Entities\Resources\Documents\DbDocumentRepository' );

    // Users
    $this->app->bind( 'JTS\Entities\Users\UserRepositoryInterface',
                      'JTS\Entities\Users\DbUserRepository' );

    $this->app->bind( 'JTS\Entities\Users\Groups\GroupRepositoryInterface',
                      'JTS\Entities\Users\Groups\DbGroupRepository' );

  }

}
