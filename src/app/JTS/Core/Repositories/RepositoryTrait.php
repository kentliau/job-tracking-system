<?php namespace JTS\Core\Repositories;

use Input;

trait RepositoryTrait {

  /**
   * @param $id
   *
   * @return mixed
   */
  public function getById($id)
  {
    return $this->model->findOrFail( $id );
  }

  public function getAll()
  {
    $models = $this->model->orderBy( 'created_at', 'desc' )
                          ->paginate( Input::has( 'limit' ) ? Input::get( 'limit' ) : 10 );

    return $models;
  }

  public function getAllInJson()
  {
    return $this->model->all()->jsonSerialize();
  }

  /**
   * @param array $attributes
   *
   * @return mixed
   */
  public function create(array $attributes)
  {
    return $this->model->create( $attributes );
  }

  /**
   * @param       $id
   * @param array $attributes
   *
   * @return mixed
   */
  public function update($id, array $attributes)
  {
    return $this->model->findOrFail( $id )->update( $attributes );
  }

  /**
   * @param $id
   *
   * @return mixed
   */
  public function destroy($id)
  {
    return $this->model->destroy( $id );
  }

  /**
   * @param $id
   *
   * @return mixed
   */
  public function restore($id)
  {
    return $this->model->restore( $id );
  }

}