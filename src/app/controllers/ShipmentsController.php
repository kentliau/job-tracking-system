<?php

use JTS\Entities\Customers\CustomerRepositoryInterface;
use JTS\Entities\Resources\Ports\PortRepositoryInterface;
use JTS\Entities\Resources\Vessels\VesselRepositoryInterface;
use JTS\Entities\Shipments\ShipmentRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class ShipmentsController extends \AbstractBaseController {

  /**
   * @var ShipmentRepositoryInterface
   */
  private $shipment;

  /**
   * @var \JTS\Entities\Customers\CustomerRepositoryInterface
   */
  private $customer;

  /**
   * @var \JTS\Entities\Resources\Ports\PortRepositoryInterface
   */
  private $port;

  /**
   * @var \VesselRepositoryInterface
   */
  private $vessel;

  /**
   * @param ShipmentRepositoryInterface                           $shipment
   * @param \JTS\Entities\Customers\CustomerRepositoryInterface   $customer
   * @param \JTS\Entities\Resources\Ports\PortRepositoryInterface $port
   * @param \VesselRepositoryInterface                            $vessel
   */
  public function __construct(ShipmentRepositoryInterface $shipment,
                              CustomerRepositoryInterface $customer,
                              PortRepositoryInterface $port,
                              VesselRepositoryInterface $vessel)
  {
    $this->shipment = $shipment;
    $this->customer = $customer;
    $this->port     = $port;
    $this->vessel = $vessel;
  }

  /**
   * Display a listing of shipments
   *
   * @return Response
   */
  public function index()
  {
    $shipments = $this->shipment->getAll();

    if ( Request::ajax() )
    {
      return $shipments;
    }

    return View::make( 'shipments.index', compact( 'shipments' ) );
  }

  /**
   * Show the form for creating a new shipment
   *
   * @return Response
   */
  public function create()
  {
    $shipment = $this->shipment->getNewDefault();

    return View::make( 'shipments.create', compact( 'shipment' ) );
  }

  /**
   * Store a newly created shipment in storage.
   *
   * @return Response
   */
  public function store()
  {
    try
    {
      $shipment = $this->shipment->create( Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.edit', [ $shipment->id ] );
  }

  /**
   * Display the specified shipment.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    return Redirect::route( 'shipments.edit', [ $id ] );
  }

  /**
   * Display the specified shipment via qualified file number.
   *
   * @param $typeAndYear
   * @param $fileNumber
   *
   * @return Response
   */
  public function showByFileNumber($typeAndYear, $fileNumber)
  {
    $type       = strtolower( substr( $typeAndYear, 0, 2 ) ) === 'im' ? 'import' : 'export';
    $fileNumber = intval( $fileNumber );
    $year       = intval( substr( $typeAndYear, 3 ) ) + 2000;

    $shipment = $this->shipment->getByFileNumber( $type, $year, $fileNumber );

    return View::make( 'shipments.edit', compact( 'shipment', [ $shipment->id ] ) );
  }

  /**
   * Show the form for editing the specified shipment.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $shipment     = $this->shipment->getById( $id );

    return View::make( 'shipments.edit', compact( 'shipment' ) );
  }

  /**
   * Update the specified shipment in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    try
    {
      $this->shipment->update( $id, Input::all() );
      $shipment = $this->shipment->getById($id);
      $shipment->users()->sync(Input::get('assigned_to'));
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route( 'shipments.edit', $id )->with( 'success', trans( 'ui.updated' ) );
  }

  /**
   * Remove the specified shipment from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $this->shipment->destroy( $id );

    return Redirect::route( 'shipments.index' );
  }

  /**
   * Restore the specified shipment from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function restore($id)
  {
    $this->shipment->restore( $id );

    return Redirect::route( 'shipments.show', $id );
  }

}
