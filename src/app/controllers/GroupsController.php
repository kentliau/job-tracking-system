<?php

use JTS\Entities\Users\Groups\GroupRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class GroupsController extends \AbstractBaseController {

  private $group;

  function __construct(GroupRepositoryInterface $group)
  {
    $this->group = $group;
  }

  public function index()
  {
    return View::make('hrm.groups.index');
  }

  public function create()
  {
    return View::make('hrm.groups.create');
  }

  public function store()
  {
    return Redirect::to('groups.index');
  }

  public function show($id)
  {
    return Redirect::to('groups.edit', $id);
  }

  public function edit($id)
  {
    return View::make('hrm.groups.edit');
  }

  public function update($id)
  {
    return Redirect::to('groups.index');
  }

  public function destroy($id)
  {
    return Redirect::to('groups.index');
  }

  public function restore($id)
  {
    return Redirect::to('groups.index');
  }

}