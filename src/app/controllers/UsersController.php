<?php

use JTS\Entities\Users\Groups\GroupRepositoryInterface;
use JTS\Entities\Users\UserRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class UsersController extends \AbstractBaseController {

  private $user;

  /**
   * @var GroupRepositoryInterface
   */
  private $group;

  function __construct(UserRepositoryInterface $user, GroupRepositoryInterface $group)
  {
    $this->user = $user;
    $this->group = $group;
  }

  public function index()
  {
    if ( Request::ajax() )
    {
      return $this->user->getAllInJson();
    }

    $users = $this->user->getAll();
    return View::make('hrm.users.index', compact('users'));
  }

  public function create()
  {
    $groupSelectionListing = $this->group->getSelectionListing();

    return View::make('hrm.users.create', compact('groupSelectionListing'));
  }

  public function store()
  {
    try
    {
      // Create the user
      $user = Sentry::createUser(array(
                                   'email'     => Input::get('email'),
                                   'password'  => Input::get('password'),
                                   'first_name' => Input::get('first_name'),
                                   'last_name' => Input::get('last_name'),
                                   'activated' => true,
                                 ));

      // Find the group using the group id
      $group = Sentry::findGroupById(Input::get('group_id'));

      // Assign the group to the user
      $user->addGroup($group);
    }
    catch (Cartalyst\Sentry\Users\LoginRequiredException $e)
    {
      echo 'Login field is required.';
    }
    catch (Cartalyst\Sentry\Users\PasswordRequiredException $e)
    {
      echo 'Password field is required.';
    }
    catch (Cartalyst\Sentry\Users\UserExistsException $e)
    {
      echo 'User with this login already exists.';
    }
    catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e)
    {
      echo 'Group was not found.';
    }

    return Redirect::route('users.index');
  }

  public function show($id)
  {
    return Redirect::route('users.edit', $id);
  }

  public function edit($id)
  {
    $groupSelectionListing = $this->group->getSelectionListing();
    $user = Sentry::findUserById($id);
    return View::make('hrm.users.edit', compact('user', 'groupSelectionListing'));
  }

  public function update($id)
  {
    try
    {
      $this->user->update( $id, Input::all() );
      $user = $this->user->getById($id);
      $user->groups()->sync([Input::get('group_id')]);
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route('users.index');
  }

  public function destroy($id)
  {
    $this->user->destroy($id);
    return Redirect::route('users.index');
  }

  public function restore($id)
  {
    $this->user->restore($id);
    return Redirect::route('users.index');
  }

}