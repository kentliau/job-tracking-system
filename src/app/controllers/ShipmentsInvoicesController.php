<?php

use JTS\Entities\Shipments\ShipmentRepositoryInterface;
use JTS\Entities\Shipments\Invoices\InvoiceRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class ShipmentsInvoicesController extends \AbstractBaseController {

  /**
   * @var ShipmentRepositoryInterface
   */
  private $shipment;

  /**
   * @var InvoiceRepositoryInterface
   */
  private $invoice;

  /**
   * @param ShipmentRepositoryInterface $shipment
   * @param InvoiceRepositoryInterface  $invoice
   */
  function __construct(ShipmentRepositoryInterface $shipment,
                       InvoiceRepositoryInterface $invoice)
  {
    $this->shipment = $shipment;
    $this->invoice  = $invoice;
  }

  /**
   * Display a listing of shipment's invoices
   *
   * @param $shipmentId
   *
   * @return Response
   */
  public function index($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $invoices = $this->invoice->getShipmentInvoices( $shipment );

    return View::make( 'shipments.invoices.index', compact( 'invoices', 'shipment' ) );
  }

  /**
   * Show the form for creating a new shipment's invoice
   *
   * @param $shipmentId
   *
   * @return Response
   */
  public function create($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $invoice = $this->invoice->getNewDefault();

    return View::make( 'shipments.invoices.create', compact( 'shipment', 'invoice' ) );
  }

  /**
   * Store a newly created shipment's invoice in storage.
   *
   * @param $shipmentId
   *
   * @return Response
   */
  public function store($shipmentId)
  {
    try
    {
      $shipment = $this->shipment->getById( $shipmentId );
      $this->invoice->create( $shipmentId, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.invoices.index', [ $shipment->id ] )
                   ->with( 'success', trans( 'ui.created' ) );
  }

  /**
   * Display the specified shipment's invoice.
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function show($shipmentId, $invoiceId)
  {
    $shipment = $this->shipment->getById($shipmentId);
    $invoice  = $this->invoice->getById($invoiceId);

    return View::make('shipments.invoices.pdf', compact('shipment', 'invoice'));
  }

  /**
   * Show the form for editing the specified shipment's invoice.
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function edit($shipmentId, $invoiceId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $invoice  = $this->invoice->getById( $invoiceId );

    return View::make( 'shipments.invoices.edit', compact( 'invoice', 'shipment' ) );
  }

  /**
   * Update the specified shipment's invoice in storage.
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function update($shipmentId, $invoiceId)
  {
    try
    {
      // \JTS\Entities\Shipments\Shipment::findOrFail($shipmentId)
      //                                 ->invoices()
      //                                 ->findOrFail($invoiceId)
      //                                 ->update(Input::all());
      $shipment = $this->shipment->getById( $shipmentId );
      $this->invoice->update($invoiceId, Input::all());

    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.invoices.index', [ $shipmentId ] )
                   ->with( 'success', trans( 'ui.updated' ) );
  }

  /**
   * Remove the specified shipment's invoice from storage.
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function destroy($shipmentId, $invoiceId)
  {
    $this->invoice->destroy( $invoiceId );

    return Redirect::route( 'shipments.invoices.index', [ $shipmentId ] )
                   ->with( 'success', trans( 'ui.deleted' ) );
  }

  /**
   * Restore
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return mixed
   */
  public function restore($shipmentId, $invoiceId)
  {
    $this->invoice->restore( $invoiceId );

    return Redirect::route( 'shipments.invoices.edit', [ $shipmentId, $invoiceId ] )
                   ->with( 'success', trans( 'ui.restored' ) );
  }

}
