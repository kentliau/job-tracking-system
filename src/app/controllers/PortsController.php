<?php

use JTS\Entities\Resources\Ports\PortRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class PortsController extends \AbstractBaseController {

  /**
   * @var PortRepositoryInterface
   */
  private $port;

  function __construct(PortRepositoryInterface $port)
  {
    $this->port = $port;
  }

  /**
   * Display a listing of ports
   *
   * @return Response
   */
  public function index()
  {
    if ( Request::ajax() )
    {
      return $this->port->getAllInJson();
    }
    $ports = $this->port->getAll();

    return View::make( 'resources.ports.index', compact( 'ports' ) );
  }

  /**
   * Show the form for creating a new port
   *
   * @return Response
   */
  public function create()
  {
    return View::make( 'resources.ports.create' );
  }

  /**
   * Store a newly created port in storage.
   *
   * @return Response
   */
  public function store()
  {
    try
    {
      $port = $this->port->create( Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    if(Request::ajax())
    {
      return $port;
    }

    return Redirect::route( 'ports.index' );
  }

  /**
   * Display the specified port.
   *
   * @param  int $id
   *
   * @return Redirect
   */
  public function show($id)
  {
    return Redirect::route( 'ports.edit', [ $id ] );
  }

  /**
   * Show the form for editing the specified port.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $port = $this->port->getById( $id );

    return View::make( 'resources.ports.edit', compact( 'port' ) );
  }

  /**
   * Update the specified port in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    try
    {
      $this->port->update( $id, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route( 'ports.index' );
  }

  /**
   * Remove the specified port from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $this->port->destroy( $id );

    return Redirect::route( 'ports.index' );
  }

  /**
   * Restore the specified port from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function restore($id)
  {
    $this->port->restore( $id );

    return Redirect::route( 'ports.index', [ $id ] );
  }

}
