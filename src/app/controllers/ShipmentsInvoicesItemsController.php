<?php

use JTS\Entities\Shipments\ShipmentRepositoryInterface;
use JTS\Entities\Shipments\Invoices\InvoiceRepositoryInterface;
use JTS\Entities\Shipments\Invoices\Items\ItemRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class ShipmentsInvoicesItemsController extends \AbstractBaseController {


  /**
   * @var JTS\Entities\Shipments\ShipmentRepositoryInterface
   */
  private $shipment;

  /**
   * @var JTS\Entities\Shipments\Invoices\InvoiceRepositoryInterface
   */
  private $invoice;

  /**
   * @var JTS\Entities\Shipments\Invoices\Items\ItemRepositoryInterface
   */
  private $item;

  /**
   * @param ShipmentRepositoryInterface $shipment
   * @param InvoiceRepositoryInterface  $invoice
   * @param ItemRepositoryInterface     $item
   */
  function __construct(ShipmentRepositoryInterface $shipment,
                       InvoiceRepositoryInterface $invoice,
                       ItemRepositoryInterface $item)
  {

    $this->shipment = $shipment;
    $this->invoice  = $invoice;
    $this->item     = $item;
  }

  /**
   * Display a listing of shipment invoice items
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function index($shipmentId, $invoiceId)
  {
    $invoice  = $this->invoice->getById( $invoiceId );
    $items    = $this->item->getInvoiceItems( $invoice );

    return View::make( 'shipments.invoices.items.index', compact( 'items', 'shipmentId', 'invoiceId' ) );
  }

  /**
   * Show the form for creating a new shipment invoice items
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function create($shipmentId, $invoiceId)
  {
    return View::make( 'shipments.invoices.items.create', compact( 'shipmentId', 'invoiceId' ) );
  }

  /**
   * Store a newly created shipment invoice items in storage.
   *
   * @param $shipmentId
   * @param $invoiceId
   *
   * @return Response
   */
  public function store($shipmentId, $invoiceId)
  {
    try
    {
      $this->item->create( $invoiceId, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.invoices.items.index', [ $shipmentId, $invoiceId ] )
                   ->with( 'success', trans( 'ui.created' ) );
  }

  /**
   * Display the specified shipment invoice items.
   *
   * @param $shipmentId
   * @param $invoiceId
   * @param $itemId
   *
   * @return Response
   */
  public function show($shipmentId, $invoiceId, $itemId)
  {
    return Redirect::route( 'shipments.invoices.items.edit', [ $shipmentId, $invoiceId, $itemId ] );
  }

  /**
   * Show the form for editing the specified shipment invoice items.
   *
   * @param $shipmentId
   * @param $invoiceId
   * @param $itemId
   *
   * @return Response
   */
  public function edit($shipmentId, $invoiceId, $itemId)
  {
    $item = $this->item->getById( $itemId );

    return View::make( 'shipments.invoices.items.edit', compact( 'item', 'shipmentId', 'invoiceId' ) );
  }

  /**
   * Update the specified shipment invoice items in storage.
   *
   * @param $shipmentId
   * @param $invoiceId
   * @param $itemId
   *
   * @return Response
   */
  public function update($shipmentId, $invoiceId, $itemId)
  {
    try
    {
      $this->item->update( $itemId, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.invoices.items.index', [ $shipmentId, $invoiceId ] )
                   ->with( 'success', trans( 'ui.updated' ) );
  }

  /**
   * Remove the specified shipment invoice items from storage.
   *
   * @param $shipmentId
   * @param $invoiceId
   * @param $itemId
   *
   * @return Response
   */
  public function destroy($shipmentId, $invoiceId, $itemId)
  {
    $this->item->destroy( $itemId );

    return Redirect::route( 'shipments.invoices.items.index', [ $shipmentId, $invoiceId ] )
                   ->with( 'success', trans( 'ui.deleted' ) );
  }

  /**
   * Restore
   *
   * @param $shipmentId
   * @param $invoiceId
   * @param $itemId
   *
   * @return mixed
   */
  public function restore($shipmentId, $invoiceId, $itemId)
  {
    $this->item->restore( $itemId );

    return Redirect::route( 'shipments.invoices.items.edit', [ $shipmentId, $invoiceId, $itemId ] )
                   ->with( 'success', trans( 'ui.restored' ) );;
  }

}
