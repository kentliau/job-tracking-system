<?php

use JTS\Entities\Shipments\ShipmentRepositoryInterface;
use JTS\Entities\Shipments\Documents\DocumentRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class ShipmentsDocumentsController extends \AbstractBaseController {


  /**
   * @var \JTS\Entities\Shipments\ShipmentRepositoryInterface
   */
  private $shipment;

  /**
   * @var \JTS\Entities\Shipments\Documents\DocumentRepositoryInterface
   */
  private $document;

  /**
   * @param \JTS\Entities\Shipments\ShipmentRepositoryInterface           $shipment
   * @param \JTS\Entities\Shipments\Documents\DocumentRepositoryInterface $document
   */
  function __construct(ShipmentRepositoryInterface $shipment,
                       DocumentRepositoryInterface $document)
  {
    $this->shipment = $shipment;
    $this->document = $document;
  }

  /**
   * Display a listing of shipment's documents
   *
   * @param $shipmentId
   *
   * @return Response
   */
  public function index($shipmentId)
  {
    $shipment  = $this->shipment->getById( $shipmentId );
    $documents = $this->document->getShipmentDocuments( $shipment );

    return View::make( 'shipments.documents.index', compact( 'documents', 'shipment' ) );
  }

  /**
   * Show the form for creating a new shipment's document
   *
   * @param $shipmentId
   *
   * @return Response
   */
  public function create($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );

    return View::make( 'shipments.documents.create', compact( 'shipment' ) );
  }

  /**
   * Store a newly created shipment's document in storage.
   *
   * @param $shipmentId
   *
   * @return Response
   */
  public function store($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );

    try
    {
      $this->document->create( $shipmentId, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.documents.index', [ $shipment->id ] )
                   ->with( 'success', trans( 'ui.created' ) );
  }

  /**
   * Display the specified shipment's document.
   *
   * @param $shipmentId
   * @param $documentId
   *
   * @return Response
   */
  public function show($shipmentId, $documentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );

    if ( ! Input::has( 'download' ) )
    {
      return Redirect::route( 'shipments.documents.edit', [ $shipmentId, $documentId ] );
    }

    $fileNumber = $this->shipment->getById( $shipmentId )->present()->fileNumber;
    $fileNumber = str_replace( "/", "_", $fileNumber );
    $document   = $this->document->getById( $documentId );

    $archiveName = '';

    $archiveName .= $fileNumber . '_';

    if ( ! empty( $document->name ) )
    {
      $archiveName .= $document->name . '_';
    }

    $archiveName .= $document->document->name;

    if ( ! empty( $document->serial_number ) )
    {
      $archiveName .= '_' . $document->serial_number;
    }

    // $archiveName .= 'id' . $documentId;
    $archiveName = str_replace( " ", "_", $archiveName );

    //$pathToFiles = storage_path() . "/jts/shipments/{$shipmentId}/documents/{$documentId}/";
    $pathToFilesForArchive = storage_path() . "/jts/shipments/{$shipmentId}/documents/{$documentId}/";
    $pathOfArchive         = storage_path() . "/jts/shipments_archive/{$shipmentId}/documents/{$archiveName}.zip";

    $zipper = new \Chumper\Zipper\Zipper;
    $zipper->make( $pathOfArchive )->add( $pathToFilesForArchive );
    $zipper->close();

    //$files = \File::allFiles($pathToFiles);

    return Response::download( $pathOfArchive );
  }

  /**
   * Show the form for editing the specified shipment document.
   *
   * @param $shipmentId
   * @param $documentId
   *
   * @return Response
   */
  public function edit($shipmentId, $documentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $document = $this->document->getById( $documentId );

    $pathToFile = storage_path() . "/jts/shipments/{$shipmentId}/documents/{$documentId}/";

    $files = is_dir( $pathToFile ) ? \File::allFiles( $pathToFile ) : null;

    return View::make( 'shipments.documents.edit', compact( 'document', 'shipment', 'files' ) );
  }

  /**
   * Update the specified shipment document in storage.
   *
   * @param $shipmentId
   * @param $documentId
   *
   * @return Response
   */
  public function update($shipmentId, $documentId)
  {
    $shipment  = $this->shipment->getById( $shipmentId );

    try
    {
      $document = $this->document->update( $shipmentId, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.documents.edit', [ $shipment->id, $document->id ] )
                   ->with( 'success', trans( 'ui.updated' ) );
  }

  /**
   * Remove the specified shipment document from storage.
   *
   * @param $shipmentId
   * @param $documentId
   *
   * @return Response
   */
  public function destroy($shipmentId, $documentId)
  {
    $shipment  = $this->shipment->getById( $shipmentId );
    $this->document->destroy( $documentId );

    return Redirect::route( 'shipments.documents.index', [ $shipment->id ] )
                   ->with( 'success', trans( 'ui.deleted' ) );
  }

  /**
   * Restore
   *
   * @param $shipmentId
   * @param $documentId
   *
   * @return mixed
   */
  public function restore($shipmentId, $documentId)
  {
    $shipment  = $this->shipment->getById( $shipmentId );
    $this->document->restore( $documentId );

    return Redirect::route( 'shipments.documents.edit', [ $shipment->id, $documentId ] )
                   ->with( 'success', trans( 'ui.restored' ) );
  }

}
