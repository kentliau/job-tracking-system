<?php

use JTS\Entities\Customers\CustomerRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class CustomersController extends \AbstractBaseController {

  /**
   * @var CustomerRepositoryInterface
   */
  private $customer;

  function __construct(CustomerRepositoryInterface $customer)
  {
    $this->customer = $customer;
  }


  /**
   * Display a listing of customers
   *
   * @return Response
   */
  public function index()
  {
    if ( Request::ajax() )
    {
      return $this->customer->getAllInJson();
    }

    if(Input::has('export'))
    {
        $table = $this->customer->getAll();
        $output='';
        foreach ($table as $row) {
            $output.=  implode(",",$row->toArray());
        }
        $headers = array(
            'Content-Type' => 'text/csv',
            'Content-Disposition' => 'attachment; filename="customers.csv"',
        );

        return Response::make(rtrim($output, "\n"), 200, $headers);
    }

    $customers = $this->customer->getAll();

    return View::make( 'crm.customers.index', compact( 'customers' ) );
  }

  /**
   * Show the form for creating a new customer
   *
   * @return Response
   */
  public function create()
  {
    return View::make( 'crm.customers.create' );
  }

  /**
   * Store a newly created customer in storage.
   *
   * @return Redirect
   */
  public function store()
  {
    try
    {
      $customer = $this->customer->create( Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    if(Request::ajax())
    {
      return $customer;
    }

    return Redirect::route( 'customers.index' );
  }

  /**
   * Display the specified customer.
   *
   * @param  int $id
   *
   * @return Redirect
   */
  public function show($id)
  {
    return Redirect::route( 'customers.edit', [ $id ] );
  }

  /**
   * Show the form for editing the specified customer.
   *
   * @param  int $id
   *
   * @return View
   */
  public function edit($id)
  {
    $customer = $this->customer->getById( $id );

    return View::make( 'crm.customers.edit', compact( 'customer' ) );
  }

  /**
   * Update the specified customer in storage.
   *
   * @param  int $id
   *
   * @return Redirect
   */
  public function update($id)
  {
    try
    {
      $this->customer->update( $id, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route( 'customers.index' );
  }

  /**
   * Remove the specified customer from storage.
   *
   * @param  int $id
   *
   * @return Redirect
   */
  public function destroy($id)
  {
    $this->customer->destroy( $id );

    return Redirect::route( 'customers.index' );
  }

  /**
   * Restore the specified customer from storage.
   *
   * @param  int $id
   *
   * @return Redirect
   */
  public function restore($id)
  {
    $this->customer->restore( $id );

    return Redirect::route( 'customers.index', [ $id ] );
  }

}
