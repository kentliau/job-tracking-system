<?php

use JTS\Core\Exceptions\ValidationException;

class SessionsController extends \AbstractBaseController {

  public function index()
  {
    // probably not so useful to display all the currently active session
  }

  public function create()
  {
    // show the login page, probably not so useful
  }

  public function store()
  {
    try
    {
      // Login credentials
      $credentials = array(
        'email'    => Input::get('email'),
        'password' => Input::get('password'),
      );

      // Authenticate the user
      $user = Sentry::authenticate( $credentials, Input::has('remember_me') ? Input::get('remember_me') : false  );
    }
    catch ( Cartalyst\Sentry\Users\LoginRequiredException $e )
    {
      return Redirect::back()
                     ->withErrors( 'Login field is required.' )
                     ->withInput();
    }
    catch ( Cartalyst\Sentry\Users\PasswordRequiredException $e )
    {
      return Redirect::back()
                     ->withErrors( 'Password field is required.' )
                     ->withInput();
    }
    catch ( Cartalyst\Sentry\Users\WrongPasswordException $e )
    {
      return Redirect::back()
                     ->withErrors( 'Wrong password, try again.' )
                     ->withInput();
    }
    catch ( Cartalyst\Sentry\Users\UserNotFoundException $e )
    {
      return Redirect::back()
                     ->withErrors( 'User was not found.' )
                     ->withInput();
    }
    catch ( Cartalyst\Sentry\Users\UserNotActivatedException $e )
    {
      return Redirect::back()
                     ->withErrors( 'User is not activated.' )
                     ->withInput();
    }

    // The following is only required if the throttling is enabled
    catch ( Cartalyst\Sentry\Throttling\UserSuspendedException $e )
    {
      return Redirect::back()
                     ->withErrors( 'User is suspended.' )
                     ->withInput();
    }
    catch ( Cartalyst\Sentry\Throttling\UserBannedException $e )
    {
      return Redirect::back()
                     ->withErrors( 'User is banned.' )
                     ->withInput();
    }

    return Redirect::route('dashboard');
  }

  public function show($id)
  {
    // show the information of the session, probably not so useful
  }

  public function edit($id)
  {
    // edit the sessions, not so useful
  }

  public function destroy()
  {
    Sentry::logout();
    return Redirect::route('home');
  }

}