<?php

use JTS\Entities\Resources\Documents\DocumentRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class DocumentsController extends \AbstractBaseController {

  /**
   * @var DocumentRepositoryInterface
   */
  private $document;

  function __construct(DocumentRepositoryInterface $document)
  {
    $this->document = $document;
  }

  /**
   * Display a listing of documents
   *
   * @return Response
   */
  public function index()
  {
    $documents = $this->document->getAll();

    return View::make( 'resources.documents.index', compact( 'documents' ) );
  }

  /**
   * Show the form for creating a new document
   *
   * @return Response
   */
  public function create()
  {
    return View::make( 'resources.documents.create' );
  }

  /**
   * Store a newly created document in storage.
   *
   * @return Response
   */
  public function store()
  {
    try
    {
      $this->document->create( Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route( 'documents.index' );
  }

  /**
   * Display the specified document.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    return Redirect::route( 'documents.edit', [ $id ] );
  }

  /**
   * Show the form for editing the specified document.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $document = $this->document->getById( $id );

    return View::make( 'resources.documents.edit', compact( 'document' ) );
  }

  /**
   * Update the specified document in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    try
    {
      $this->document->update( $id, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route( 'documents.index' );
  }

  /**
   * Remove the specified document from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $this->document->destroy( $id );

    return Redirect::route( 'documents.index' );
  }

  /**
   * Restore the specified document from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function restore($id)
  {
    $this->document->restore( $id );

    return Redirect::route( 'documents.index', [ $id ] );
  }

}
