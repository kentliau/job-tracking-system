<?php

use JTS\Entities\Shipments\ShipmentRepositoryInterface;

class ShipmentsTasksController extends \AbstractBaseController {

  private $shipment;

  function __construct(ShipmentRepositoryInterface $shipment)
  {
    $this->shipment = $shipment;
  }

  /**
   * Display a listing of tasks
   *
   * @return Response
   */
  public function index($shipmentId)
  {
    $shipment = $this->shipment->getById($shipmentId);

    return View::make( 'shipments.tasks.index', compact('shipment' ) );
  }

  /**
   * Show the form for creating a new task
   *
   * @return Response
   */
  public function create()
  {
  }

  /**
   * Store a newly created task in storage.
   *
   * @return Response
   */
  public function store()
  {
  }

  /**
   * Display the specified task.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified task.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified task in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
  }

  /**
   * Remove the specified task from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
  }

}
