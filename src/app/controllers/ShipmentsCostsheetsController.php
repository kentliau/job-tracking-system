<?php

use JTS\Entities\Shipments\ShipmentRepositoryInterface;

class ShipmentsCostsheetsController extends \AbstractBaseController {

  private $shipment;

  function __construct(ShipmentRepositoryInterface $shipment)
  {
    $this->shipment = $shipment;
  }

  /**
   * Display a listing of costsheets
   *
   * @return Response
   */
  public function index($shipmentId)
  {
    $shipment = $this->shipment->getById($shipmentId);

    return View::make( 'shipments.costsheets.index', compact( 'shipment' ) );
  }

  /**
   * Show the form for creating a new costsheet
   *
   * @return Response
   */
  public function create()
  {
  }

  /**
   * Store a newly created costsheet in storage.
   *
   * @return Response
   */
  public function store()
  {
  }

  /**
   * Display the specified costsheet.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
  }

  /**
   * Show the form for editing the specified costsheet.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
  }

  /**
   * Update the specified costsheet in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
  }

  /**
   * Remove the specified costsheet from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
  }

}
