<?php

class HomeController extends \AbstractBaseController {

  public function index()
  {
    if ( Sentry::check() )
    {
      return Redirect::route('dashboard');
    }

    return View::make( 'home' );
  }

}
