<?php

use JTS\Entities\Shipments\ShipmentRepositoryInterface;
use JTS\Entities\Shipments\Items\ItemRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class ShipmentsItemsController extends \AbstractBaseController {


  /**
   * @var \JTS\Entities\Shipments\ShipmentRepositoryInterface
   */
  private $shipment;

  /**
   * @var \JTS\Entities\Shipments\Items\ItemRepositoryInterface
   */
  private $item;


  /**
   * @param \JTS\Entities\Shipments\ShipmentRepositoryInterface   $shipment
   * @param \JTS\Entities\Shipments\Items\ItemRepositoryInterface $item
   */
  function __construct(ShipmentRepositoryInterface $shipment,
                       ItemRepositoryInterface $item)
  {
    $this->shipment = $shipment;
    $this->item     = $item;
  }

  /**
   * Display a listing of shipment's items
   *
   * @param int $shipmentId
   *
   * @return Response
   */
  public function index($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $items    = $this->item->getAllOfShipment( $shipment );

    return View::make( 'shipments.items.index', compact( 'items', 'shipment' ) );
  }

  /**
   * Show the form for creating a new shipment's item
   *
   * @param int $shipmentId
   *
   * @return View
   */
  public function create($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );

    return View::make( 'shipments.items.create', compact( 'shipment' ) );
  }

  /**
   * Store a newly created shipment's item in storage.
   *
   * @param int $shipmentId
   *
   * @return Response
   */
  public function store($shipmentId)
  {
    $shipment = $this->shipment->getById( $shipmentId );

    try
    {
      $this->item->create( $shipmentId, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.items.index', [ $shipment->id ] )
                   ->with( 'success', trans( 'ui.created' ) );
  }

  /**
   * Display the specified shipment's item.
   *
   * @param int $shipmentId
   * @param int $itemId
   *
   * @return Response
   */
  public function show($shipmentId, $itemId)
  {
    return Redirect::route( 'shipments.items.edit', [ $shipmentId, $itemId ] );
  }

  /**
   * Show the form for editing the specified shipment's item.
   *
   * @param $shipmentId
   * @param $itemId
   *
   * @return Response
   */
  public function edit($shipmentId, $itemId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $item     = $this->item->getById( $itemId );

    return View::make( 'shipments.items.edit', compact( 'item', 'shipment' ) );
  }

  /**
   * Update the specified shipment's item in storage.
   *
   * @param $shipmentId
   * @param $itemId
   *
   * @return Response
   */
  public function update($shipmentId, $itemId)
  {
    try
    {

      $shipment = $this->shipment->getById($shipmentId);
      //$item      = $this->item->getShipmentItemById($item, $shipment);

      // \JTS\Entities\Shipments\Shipment::findOrFail($shipmentId)
      //         ->items()
      //         ->findOrFail($itemId)
      //         ->update(Input::all());

      $this->item->update($itemId, Input::all());

    }
    catch ( ValidationException $e )
    {
      return Redirect::back()
                     ->withErrors( $e->getValidator() )
                     ->withInput();
    }

    return Redirect::route( 'shipments.items.index', [ $shipment->id ] )
                   ->with( 'success', trans( 'ui.updated' ) );
  }

  /**
   * Remove the specified shipment item from storage.
   *
   * @param $shipmentId
   * @param $itemId
   *
   * @return Response
   */
  public function destroy($shipmentId, $itemId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $this->item->destroy( $itemId );

    return Redirect::route( 'shipments.items.index', [ $shipment->id ] );
  }

  /**
   * Restore
   *
   * @param $shipmentId
   * @param $itemId
   *
   * @return mixed
   */
  public function restore($shipmentId, $itemId)
  {
    $shipment = $this->shipment->getById( $shipmentId );
    $this->item->restore( $itemId );

    return Redirect::route( 'shipments.items.edit', [ $shipment->id, $itemId ] );
  }

}
