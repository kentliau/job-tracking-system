<?php

use JTS\Entities\Resources\Vessels\VesselRepositoryInterface;
use JTS\Core\Exceptions\ValidationException;

class VesselsController extends \AbstractBaseController {

  /**
   * @var VesselRepositoryInterface
   */
  private $vessel;

  function __construct(VesselRepositoryInterface $vessel)
  {
    $this->vessel = $vessel;
  }

  /**
   * Display a listing of vessels
   *
   * @return Response
   */
  public function index()
  {
    if ( Request::ajax() )
    {
      return $this->vessel->getAllInJson();
    }
    $vessels = $this->vessel->getAll();

    return View::make( 'resources.vessels.index', compact( 'vessels' ) );
  }

  /**
   * Show the form for creating a new vessel
   *
   * @return Response
   */
  public function create()
  {
    return View::make( 'resources.vessels.create' );
  }

  /**
   * Store a newly created vessel in storage.
   *
   * @return Response
   */
  public function store()
  {

    try
    {
      $vessel = $this->vessel->create( Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    if(Request::ajax())
    {
      return $vessel;
    }

    return Redirect::route( 'resources.vessels.index' );
  }

  /**
   * Display the specified vessel.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function show($id)
  {
    return Redirect::route( 'vessels.edit', [ $id ] );
  }

  /**
   * Show the form for editing the specified vessel.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function edit($id)
  {
    $vessel = $this->vessel->getById( $id );

    return View::make( 'resources.vessels.edit', compact( 'vessel' ) );
  }

  /**
   * Update the specified vessel in storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function update($id)
  {
    try
    {
      $this->vessel->update( $id, Input::all() );
    }
    catch ( ValidationException $e )
    {
      return Redirect::back()->withErrors( $e->getValidator() )->withInput();
    }

    return Redirect::route( 'vessels.index' );
  }

  /**
   * Remove the specified vessel from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function destroy($id)
  {
    $this->vessel->destroy( $id );

    return Redirect::route( 'vessels.index' );
  }

  /**
   * Restore the specified vessel from storage.
   *
   * @param  int $id
   *
   * @return Response
   */
  public function restore($id)
  {
    $this->vessel->restore( $id );

    return Redirect::route( 'vessels.index', [ $id ] );
  }

}
