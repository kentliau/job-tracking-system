<?php

class GroupsTableSeeder extends Seeder {

  public function run()
  {

//    Sentry::createGroup( [ 'name'        => 'Administrator',
//                           'permissions' => [
//                             'administration'       => 1,
//                             'shipments.create'     => 1,
//                             'shipments.read'       => 1,
//                             'shipments.read_all'   => 1,
//                             'shipments.update'     => 1,
//                             'shipments.update_all' => 1,
//                             'shipments.delete'     => 1,
//                             'shipments.delete_all' => 1,
//                           ],
//                         ] );
//
//    Sentry::createGroup( [ 'name'        => 'Operator',
//                           'permissions' => [
//                             'administration'       => 0,
//                             'shipments.create'     => 1,
//                             'shipments.read'       => 1,
//                             'shipments.read_all'   => 1,
//                             'shipments.update'     => 1,
//                             'shipments.update_all' => 1,
//                             'shipments.delete'     => 1,
//                             'shipments.delete_all' => 1,
//                           ],
//                         ] );
    try
    {
      Sentry::createGroup( array(
                             'name'        => 'Superuser',
                             'permissions' => array(
                               'superuser' => 1
                             ),
                           ) );

      Sentry::createGroup( array(
                             'name'        => 'Administrator',
                             'permissions' => array(
                               'admin'    => 1,
                               'operator' => 1,
                             ),
                           ) );

      Sentry::createGroup( array(
                             'name'        => 'Operator',
                             'permissions' => array(
                               'admin'    => 0,
                               'operator' => 1,
                             ),
                           ) );

    }
    catch ( Exception $e )
    {
    }

  }
}