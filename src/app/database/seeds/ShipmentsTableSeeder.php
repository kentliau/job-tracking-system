<?php

use Faker\Factory as Faker;
use JTS\App as JTS;
use JTS\Entities\Shipments\Shipment;

class ShipmentsTableSeeder extends Seeder {

  public function run()
  {
    $faker    = Faker::create();
    $shipmentRepo = App::make( 'JTS\Entities\Shipments\ShipmentRepositoryInterface' );

    foreach ( range( 50, 1 ) as $index )
    {
      $type = $faker->randomElement( JTS::$type );

      $shipment = Shipment::create( [
        'file_number'            => $shipmentRepo->getNextFileNumber( $type ),
        'file_year'              => 2014,
        'file_date'              => Carbon\Carbon::now()->subDays( $index ),
        'type'                   => $type,
        'status'                 => $faker->randomElement( JTS::$status ),
        'priority'               => $faker->randomElement( JTS::$priority ),
        'shipper_id'             => $faker->numberBetween( 1, 10 ),
        'consignee_id'           => $faker->numberBetween( 1, 10 ),
        'description'            => $faker->realText( 500, 5 ),
        'load_port_id'           => $faker->numberBetween( 1, 10 ),
        'discharge_port_id'      => $faker->numberBetween( 1, 10 ),
        'vessel_id'              => $faker->numberBetween( 1, 10 ),
        'vessel_lot'             => strtoupper( $faker->bothify( '???##' ) ),
        'departure_date'         => Carbon\Carbon::now()->addDays( $index ),
        'estimated_arrival_date' => Carbon\Carbon::now()->addDays( $index + 7 ),
        'arrived_date'           => Carbon\Carbon::now()->addDays( $index + 7 ),
        'bill_of_lading_number'  => strtoupper( $faker->bothify( '#########' ) ),
      ] );

      $shipment->users()->attach($faker->numberBetween( 1, 4 ));

    }
  }

}