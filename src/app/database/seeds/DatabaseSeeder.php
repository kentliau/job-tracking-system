<?php

class DatabaseSeeder extends Seeder {

  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Eloquent::unguard();

    $this->call( 'GroupsTableSeeder' );
    $this->call( 'UsersTableSeeder' );

    $this->call( 'CustomersTableSeeder' );
    $this->call( 'DocumentsTableSeeder' );
    $this->call( 'PortsTableSeeder' );
    $this->call( 'VesselsTableSeeder' );

    $this->call( 'ShipmentsTableSeeder' );
    $this->call( 'ShipmentsItemsTableSeeder' );
    $this->call( 'ShipmentsDocumentsTableSeeder' );
    $this->call( 'ShipmentsInvoicesTableSeeder' );
    $this->call( 'ShipmentsInvoicesItemsTableSeeder' );

  }

}
