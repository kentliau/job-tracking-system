<?php

use Faker\Factory as Faker;
use JTS\Entities\Resources\Documents\Document;

class DocumentsTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    $documents = [ 'Invoice',
                    'Packing List / Weight List',
                    'Marine Insurance',
                    'Bill Of Lading (B/L)',
                    'Preferential Duty Exemption',
                    'Fumigation Certificate',
                    'Import Permit (AP)',
                    'Export Permit',
                    'Rebuilt Documents',
                    'Shipping Instructions',
                    'LOA',
                    'EDO',
                    'IID',
                    'NOA',
                    'K1',
                    'K2',
                    'K3',
                    'K8',
                    'K9',
                    'ZB1',
                    'ZB2',
                    'ZB3',
                    'ZB4'];

    foreach ( range( 0, count($documents) - 1 ) as $index )
    {
      Document::firstOrCreate( [
        'name'        => $documents[ $index ],
      ] );
    }
  }

}