<?php

class UsersTableSeeder extends Seeder {

  public function run()
  {

    $superuserGroup    = Sentry::findGroupByName( 'Superuser' );
    $adminGroup    = Sentry::findGroupByName( 'Administrator' );
    $operatorGroup = Sentry::findGroupByName( 'Operator' );

    try
    {
      Sentry::register( array(
                          'email'      => 'superuser@jts.app',
                          'password'   => '123456',
                          'first_name' => 'Superman',
                          'last_name'  => 'Utopia',
                        ), true )->addGroup( $superuserGroup );

      Sentry::register( array(
                          'email'      => 'john@appleseed.com',
                          'password'   => '123456',
                          'first_name' => 'John',
                          'last_name'  => 'Appleseed',
                        ), true )->addGroup( $superuserGroup );

      Sentry::register( array(
                          'email'      => 'admin@jts.app',
                          'password'   => '123456',
                          'first_name' => 'Admin',
                          'last_name'  => 'Isabella'
                        ), true )->addGroup( $adminGroup );

      Sentry::register( array(
                          'email'      => 'operator@jts.app',
                          'password'   => '123456',
                          'first_name' => 'Opera',
                          'last_name'  => 'Thor'
                        ), true )->addGroup( $operatorGroup );
    }
    catch(Exception $e)
    {}

  }

}