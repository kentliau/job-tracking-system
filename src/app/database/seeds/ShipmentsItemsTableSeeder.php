<?php

use Faker\Factory as Faker;
use JTS\Entities\Shipments\Items\Item;

class ShipmentsItemsTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    foreach ( range( 1, 500 ) as $index )
    {
      Item::create( [
        'shipment_id'   => $faker->numberBetween( 1, 50 ),
        'name'          => $faker->colorName(),
        'serial_number' => strtoupper( $faker->bothify( '??#####' ) ),
        'description'   => $faker->paragraph(),
      ] );
    }
  }

}