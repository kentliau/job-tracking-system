<?php

use Faker\Factory as Faker;
use JTS\Entities\Resources\Vessels\Vessel;

class VesselsTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    foreach ( range( 1, 10 ) as $index )
    {
      Vessel::create( [
        'name'        => ucfirst( $faker->domainWord() ),
        'code'        => $faker->lexify( '???' ),
        'description' => $faker->paragraph(),
      ] );
    }
  }

}