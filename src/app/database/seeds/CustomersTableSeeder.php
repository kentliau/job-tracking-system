<?php

use Faker\Factory as Faker;
use JTS\Entities\Customers\Customer;

class CustomersTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    foreach ( range( 1, 10 ) as $index )
    {
      Customer::create( [
        'name'         => $faker->company(),
        'contact_name' => $faker->name(),
        'description'  => $faker->catchPhrase(),
        'phone_number' => $faker->phoneNumber(),
        'fax_number'   => $faker->phoneNumber(),
        'email'        => $faker->safeEmail(),
        'address'      => $faker->address(),
      ] );
    }
  }

}