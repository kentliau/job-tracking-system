<?php

use Faker\Factory as Faker;
use JTS\Entities\Shipments\Invoices\Invoice;

class ShipmentsInvoicesTableSeeder extends Seeder {

  public function run()
  {
    $faker   = Faker::create();
    $invoice = App::make( 'JTS\Entities\Shipments\Invoices\InvoiceRepositoryInterface' );

    foreach ( range( 1, 200 ) as $index )
    {
      Invoice::create( [
        'shipment_id'    => $faker->numberBetween( 1, 50 ),
        'invoice_number' => $invoice->getNextInvoiceNumber(),
        'description'    => $faker->paragraph(),
      ] );
    }
  }

}