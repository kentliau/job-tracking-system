<?php

use Faker\Factory as Faker;
use JTS\Entities\Shipments\Documents\Document;

class ShipmentsDocumentsTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    foreach ( range( 1, 200 ) as $index )
    {
      Document::create( [
        'shipment_id'   => $faker->numberBetween( 1, 50 ),
        'document_id'   => $faker->numberBetween( 1, 11 ),
        'name'          => null,
        'serial_number' => strtoupper( $faker->bothify( '??#####' ) ),
      ] );
    }
  }

}