<?php

use Faker\Factory as Faker;

class PortsTableSeeder extends Seeder {

	public function run()
	{
		$faker = Faker::create();

		foreach(range(1, 10) as $index)
		{
      JTS\Entities\Resources\Ports\Port::create([
        'name'           => $faker->city(),
        'code'           => strtoupper($faker->lexify('???')),
        'description'    => $faker->paragraph(),
        'contact_name'   => $faker->name(),
        'phone_number'   => $faker->phoneNumber(),
        'fax_number'     => $faker->phoneNumber(),
        'email'          => $faker->safeEmail(),
        'address'        => $faker->address(),
			]);
		}
	}

}