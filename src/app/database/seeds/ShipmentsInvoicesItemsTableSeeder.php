<?php

use Faker\Factory as Faker;
use JTS\Entities\Shipments\Invoices\Items\Item;

class ShipmentsInvoicesItemsTableSeeder extends Seeder {

  public function run()
  {
    $faker = Faker::create();

    foreach ( range( 1, 500 ) as $index )
    {
      Item::create( [
        'invoice_id'  => $faker->numberBetween( 1, 200 ),
        'description' => $faker->catchPhrase(),
        'currency'    => 'MYR',
        'amount'      => $faker->randomFloat( 2, 1000, 10000 ),
      ] );
    }
  }

}