<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentUserTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'shipment_user', function (Blueprint $table)
    {
      $table->integer( 'shipment_id' )->unsigned();
      $table->integer( 'user_id' )->unsigned();

      // We'll need to ensure that MySQL uses the InnoDB engine to
      // support the indexes, other engines aren't affected.
      $table->engine = 'InnoDB';
      $table->primary( [ 'user_id', 'shipment_id' ] );
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'shipment_user' );
  }

}
