<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateVesselsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'vessels', function (Blueprint $table)
    {
      $table->increments( 'id' );
      $table->string( 'name' )->nullable();
      $table->string( 'code', 10 )->nullable();
      $table->text( 'description' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
      $table->index( 'name' );
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'vessels' );
  }

}
