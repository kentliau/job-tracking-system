<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentsInvoicesTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'shipments_invoices', function (Blueprint $table)
    {
      $table->increments( 'id' );
      $table->integer( 'shipment_id' )->unsigned();
      $table->integer( 'invoice_number' )->unsigned();
      $table->text( 'description' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
      $table->unique( 'invoice_number' );
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'shipments_invoices' );
  }

}
