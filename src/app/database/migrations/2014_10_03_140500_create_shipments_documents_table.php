<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentsDocumentsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'shipments_documents', function (Blueprint $table)
    {
      $table->increments( 'id' );
      $table->integer( 'shipment_id' )->unsigned();
      $table->integer( 'document_id' )->unsigned();
      $table->string( 'name' )->nullable();
      $table->string( 'serial_number' )->nullable();
      // $table->string('path')->nullable();

      // Required date data for Eloquent to work nicely
      $table->softDeletes();
      $table->timestamps();

      // We'll need to ensure that MySQL uses the InnoDB engine to
      // support the indexes, other engines aren't affected.
      $table->engine = 'InnoDB';

    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'shipments_documents' );
  }

}
