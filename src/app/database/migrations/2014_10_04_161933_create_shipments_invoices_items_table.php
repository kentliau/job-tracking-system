<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentsInvoicesItemsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'shipments_invoices_items', function (Blueprint $table)
    {
      $table->increments( 'id' );

      $table->integer( 'invoice_id' )->unsigned();
      $table->string( 'description' )->nullable();
      $table->string( 'currency', 3 )->default( 'MYR' );
      $table->float( 'amount' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'shipments_invoices_items' );
  }

}
