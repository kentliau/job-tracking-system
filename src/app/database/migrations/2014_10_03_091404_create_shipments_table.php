<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'shipments', function (Blueprint $table)
    {

      $type     = \JTS\App::$type;
      $statuses = \JTS\App::$status;
      $priority = \JTS\App::$priority;

      $table->increments( 'id' );

      $table->integer( 'file_number' )->unsigned();
      $table->smallInteger( 'file_year' )->unsigned();
      $table->dateTime( 'file_date' );

      $table->enum( 'type', $type );
      $table->enum( 'status', $statuses )->default( $statuses[ 0 ] );
      $table->enum( 'priority', $priority )->default( $priority[ 1 ] );

      $table->integer( 'shipper_id' )->unsigned()->nullable();
      // $table->foreign('shipper_id')->references('id')->on('customers');

      $table->integer( 'consignee_id' )->unsigned()->nullable();
      // $table->foreign('consignee_id')->references('id')->on('customers');

      $table->text( 'description' )->nullable();

      $table->integer( 'load_port_id' )->unsigned()->nullable();
      // $table->foreign('load_port_id')->references('id')->on('ports');

      $table->integer( 'discharge_port_id' )->unsigned()->nullable();
      // $table->foreign('discharge_port_id')->references('id')->on('ports');

      $table->integer( 'vessel_id' )->unsigned()->nullable();
      // $table->foreign('vessel_id')->references('id')->on('vessels');

      $table->string( 'vessel_lot' )->nullable();

      $table->dateTime( 'departure_date' )->nullable();
      $table->dateTime( 'estimated_arrival_date' )->nullable();
      $table->dateTime( 'arrived_date' )->nullable();

      $table->string( 'bill_of_lading_number' )->nullable();

      $table->dateTime( 'expired_at' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
      $table->unique( [ 'file_number', 'file_year', 'type' ] );
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'shipments' );
  }

}
