<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePortsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'ports', function (Blueprint $table)
    {
      $table->increments( 'id' );
      $table->string( 'name' );
      $table->string( 'code', 10 )->nullable;
      $table->text( 'description' )->nullable();
      $table->string( 'contact_name' )->nullable();
      $table->string( 'phone_number' )->nullable();
      $table->string( 'fax_number' )->nullable();
      $table->string( 'email' )->nullable();
      $table->text( 'address' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
      $table->unique( 'name' );
      $table->index( 'code' );
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'ports' );
  }

}
