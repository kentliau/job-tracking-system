<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomersTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'customers', function (Blueprint $table)
    {
      $table->increments( 'id' );
      $table->string( 'name' );
      $table->string( 'contact_name' )->nullable();
      $table->text( 'description' )->nullable();
      $table->string( 'phone_number' )->nullable();
      $table->string( 'fax_number' )->nullable();
      $table->string( 'email' )->nullable();
      $table->text( 'address' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
      $table->index( 'name' );
      $table->index( 'contact_name' );
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'customers' );
  }

}
