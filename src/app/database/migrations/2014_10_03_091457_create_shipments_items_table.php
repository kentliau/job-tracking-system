<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateShipmentsItemsTable extends Migration {

  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create( 'shipments_items', function (Blueprint $table)
    {
      $table->increments( 'id' );
      $table->integer( 'shipment_id' )->unsigned();
      $table->string( 'name' );
      $table->string( 'serial_number' )->nullable();
      $table->text( 'description' )->nullable();

      // Required date data for Eloquent to work nicely
      $table->timestamps();
      $table->softDeletes();

      // Make sure it use InnoDB, not others
      $table->engine = 'InnoDB';
    } );
  }


  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::drop( 'shipments_items' );
  }

}
