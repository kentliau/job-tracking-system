<?php

return array(

  'all' => 'All',

  'ok'     => 'Ok',
  'cancel' => 'Cancel',
  'yes'    => 'Yes',
  'no'     => 'No',
  'back'   => 'Back',

  'action' => 'Action',

  'save'      => 'Save',
  'saving'    => 'Saving',
  'saved'     => 'Saved',

  'update'    => 'Update',
  'updated'   => 'Updated',
  'updating'  => 'Updating',

  'create'    => 'Create',
  'created'   => 'Created',
  'creating'  => 'Creating',

  'delete'    => 'Delete',
  'deleted'   => 'Deleted',
  'deleting'  => 'Deleting',

  'restore'   => 'Restore',
  'restored'  => 'Restored',
  'restoring' => 'Restoring',

  'reset'        => 'Reset',
  'resetting'    => 'Resetting',
  'undo_changes' => 'Undo Changes',

  'clear'     => 'Clear',
  'clearing'  => 'Clearing',
  'cleared'   => 'Cleared',

  'preview'      => 'Preview',
  'download'     => 'Download',
  'downloads'    => 'Downloads',
  'download_all' => 'Download All',

  'search'    => 'Search',
  'searching' => 'Searching',

  'username' => 'Username',
  'email'    => 'Email',
  'password' => 'Password',

  'not_login'   => 'Not Login',
  'login'       => 'Login',
  'logout'      => 'Logout',
  'remember_me' => 'Remember me',

  'home'        => 'Home',
  'dashboard'   => 'Dashboard',
  'preferences' => 'Preferences',

  'help'      => 'Help',
  'policy'    => 'Policy',
  'cookies'   => 'Cookies',
  'legal'     => 'Legal',
  'copyright' => 'Copyright',

  'missing_field' => '-',

  // API
  'endpoints' => 'Endpoints',

  // feedback
  'congrats' => 'Congrats',
  'success' => 'Success',
  'error'   => 'Error',
  'errors'  => 'Errors',
  'warning' => 'Warning',
  'info'    => 'Info',
  'danger'  => 'Danger',

  'messages' => 'Messages',

  'in_zip' => 'In zip',


  'first_name' => 'First name',
  'last_name' => 'Last name',
  'group' => 'Group',

);