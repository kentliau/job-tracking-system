<?php

return array(

  'dashboard'                             => 'Dashboard',
  'human_resources_management'            => 'Human Resources Management',
  'human_resources_management_abbr'       => 'HRM',
  'customer_relationship_management'      => 'Customer Relationship Management',
  'customer_relationship_management_abbr' => 'CRM',
  'resources_management'                  => 'Resources Management',
  'resources_management_abbr'             => 'RM',
  'shipment_jobs'                         => 'Shipment Jobs',

  'total_company_jobs'       => 'Total Jobs',
  'total_individual_jobs'    => 'Total Jobs Currently Handling',
  'notifications'            => 'Notifications',
  'job_notifications' => 'Job Notifications',
  'job_handled_by_you'       => 'Jobs handled by You',

  'user' => 'User',
  'users' => 'Users',

  'group' => 'Group',
  'groups' => 'Groups',

  'shipment'      => 'Shipment',
  'shipments'     => 'Shipments',
  'item'          => 'Item',
  'items'         => 'Items',
  'document'      => 'Document',
  'document_type' => 'Document Type',
  'documents'     => 'Documents',
  'invoice'       => 'Invoice',
  'invoices'      => 'Invoices',
  'invoice_item'  => 'Invoice item',
  'invoice_items' => 'Invoice items',
  'vessel'        => 'Vessel',
  'vessels'       => 'Vessels',
  'port'          => 'Port',
  'ports'         => 'Ports',
  'customer'      => 'Customer',
  'customers'     => 'Customers',
  'tasks'         => 'Tasks',
  'costsheets'    => 'Costsheets',

  'file_number'                 => 'File #',
  'file_date'                   => 'File Date',
  'type'                        => 'Type',
  'status'                      => 'Status',
  'priority'                    => 'Priority',
  'shipper'                     => 'Shipper',
  'consignee'                   => 'Consignee',
  'shipper_name'                => 'Shipper Name',
  'consignee_name'              => 'Consignee Name',
  'description'                 => 'Description',
  'load_port'                   => 'Loading',
  'discharge_port'              => 'Discharge',
  'load_port_name'              => 'Loading Port Name',
  'discharge_port_name'         => 'Discharge Port Name',
  'vessel'                      => 'Vessel',
  'vessel_name'                 => 'Vessel Name',
  'vessel_lot'                  => 'Voyage #',
  'voyage_number'               => 'Voyage #',
  'departure_date'              => 'Departure At',
  'estimated_arrival_date'      => 'Estimated Arrival Date',
  'estimated_time_arrival'      => 'Estimated Time of Arrival',
  'estimated_time_deliver'      => 'Estimated Time of Deliver',
  'estimated_time_arrival_abbr' => 'ETA',
  'estimated_time_deliver_abbr' => 'ETD',
  'arrived_date'                => 'Arrived At',
  'bill_of_lading_number'       => 'Bill of Lading #',
  'bill_of_lading_number_abbr'  => 'B/L #',
  'code'                  => 'Code',

  'assigned_to' => 'Assigned to',

  'name'          => 'Name',
  'serial_number' => 'Serial #',
  'files'         => 'Files',
  'current_files' => 'Uploaded Files',

  'company_name' => 'Company name',
  'contact_name' => 'Contact name',
  'phone_number' => 'Phone number',
  'fax_number' => 'Fax number',
  'email' => 'Email',
  'address' => 'Address',

  'invoice_number' => 'Invoice #',

  'expired_at' => 'Expired At',

  'import'      => 'Import',
  'export'      => 'Export',
  'import_abbr' => 'IM',
  'export_abbr' => 'EX',

  'processing'  => 'Processing',
  'uncompleted' => 'Uncompleted',
  'completed'   => 'Completed',
  'cancelled'   => 'Cancelled',

  'low'      => 'Low',
  'standard' => 'Standard',
  'high'     => 'High',

  'select_type_of_job' => 'Select the type of Job',

  'create_new_shipment'   => 'Create new Shipment',
  'creating_new_shipment' => 'Creating a new Shipment',
  'update_shipment'       => 'Update Shipment',

  'new_export_shipment' => 'New Export Job',
  'new_import_shipment' => 'New Import Job',

  'new_user' => 'New User',
  'new_group' => 'New Group',

  'new_job' => 'New Job',
  'new_item'     => 'New Item',
  'new_document' => 'New Document',
  'new_invoice'  => 'New Invoice',
  'new_customer' => 'New Customer',
  'new_document' => 'New Document',
  'new_port' => 'New Port',
  'new_vessel' => 'New Vessel',

  'creating_item' => 'Creating item for shipment :fileNumber',
  'editing_item'  => 'Editing item of shipment :fileNumber',

  'creating_new_user' => 'Creating New User',
  'editing_user' => 'Editing New User',
  'creating_document' => 'Creating document for shipment :fileNumber',
  'editing_document'  => 'Editing document',

  'creating_new_customer' => 'Creating new Customer',
  'editing_customer' => 'Editing Customer',

  'creating_new_document' => 'Creating New Document',
  'creating_new_port'     => 'Creating New Port',
  'editing_port'          => 'Editing Port',
  'creating_new_vessel'   => 'Creating New Vessel',
  'editing_vessel'        => 'Editing Vessel',

  'export_to_csv_card' => 'Export to CSV Card',

);
