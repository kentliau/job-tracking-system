@extends('layouts.default_simple')

@section('content')

@include('partials._inspiring_quote')

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <h2>Manual for Dummy</h2>
    <h3><a href="http://google.com">Let Google Do the Job for You.</a></h3>
  </div> <!-- .col -->
</div> <!-- .row -->

@stop