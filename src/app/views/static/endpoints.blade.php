@extends('layouts.default_simple')

@section('content')

@foreach(['shipments',
          'shipments.items',
          'shipments.documents',
          'shipments.invoices',
          'shipments.invoices.items',
          'ports', 'vessels', 'documents', 'customers',
          'users', 'groups',  'sessions'] as $resource)

{{-- array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) --}}
{{-- array_pad( [], ( count( explode('.', $resource) ) ), 1 ) --}}

  <div>
    <h4>{{ $resource }}</h4>

    GET
      <a href="{{ route("{$resource}.index", array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) ) }}">
        {{ route("{$resource}.index", array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) ) }}
      </a><br>

    GET
      <a href="{{ route("{$resource}.show", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}">
        {{ route("{$resource}.show", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}
      </a><br>

    GET
      <a href="{{ route("{$resource}.create", array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) ) }}">
        {{ route("{$resource}.create", array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) ) }}
      </a><br>

    GET
      <a href="{{ route("{$resource}.edit", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}">
        {{ route("{$resource}.edit", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}
      </a><br>

    <br>

    POST
      <a href="{{ route("{$resource}.store", array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) ) }}">
        {{ route("{$resource}.store", array_pad( [], ( count( explode('.', $resource) ) - 1 ), 1 ) ) }}
      </a><br>

    PUT
      <a href="{{ route("{$resource}.update", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}">
        {{ route("{$resource}.update", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}
      </a><br>

    DELETE
      <a href="{{ route("{$resource}.destroy", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}">
        {{ route("{$resource}.destroy", array_pad( [], ( count( explode('.', $resource) ) ), 1 ) ) }}
      </a><br>
  </div>

@endforeach

@stop