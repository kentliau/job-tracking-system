@extends('layouts.default')

@section('sidebar')
  @include('shipments._create_sidebar')
  @parent
@stop

@section('content')
<div class="row">
  <div class="col-md-offset-3 col-md-6">
    <div class="well">

      {{ Form::model($shipment, ['route' => 'shipments.store', 'method' => 'post', 'class' => 'form-horizontal']) }}

        <fieldset>
          <legend>{{ trans('jts.creating_new_shipment') }}</legend>

          @include('shipments._form')

          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="pull-right">
                {{ Form::submit(trans('jts.create_new_shipment'), ['class' => 'btn btn-primary']) }}
              </div>
            </div>
          </div>
        </fieldset>

      {{ Form::close() }}

    </div> <!-- .well -->
  </div> <!-- .col -->
</div> <!-- .row -->
@stop
