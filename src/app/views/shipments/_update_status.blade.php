{{--
<div class="btn-group">
  <a href="" class="btn btn-xs btn-{{ $shipment->present()->statusClass }}">{{ trans('jts.' . $shipment->status) }}</a>
  <a href="#" class="btn btn-xs dropdown-toggle btn-{{ $shipment->present()->statusClass }}" data-toggle="dropdown"><span class="caret"></span></a>
  <ul class="dropdown-menu">
    <li><a href="">processing</a></li>
    <li><a href="">completed</a></li>
    <li><a href="">uncompleted</a></li>
    <li><a href="">cancelled</a></li>
  </ul>
</div>
--}}
<span class="dot dot-{{ $shipment->present()->statusClass }}">{{ trans('jts.' . $shipment->status) }}</span>
