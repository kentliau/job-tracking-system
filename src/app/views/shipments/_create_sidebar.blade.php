<li>
  <a href="{{ route('shipments.create', ['type' => 'export'])  }}">
    <i class="glyphicon glyphicon-plus"></i>
    {{ trans('jts.new_export_shipment') }}
  </a>
</li>

<li>
  <a href="{{ route('shipments.create', ['type' => 'import'])  }}">
    <i class="glyphicon glyphicon-plus"></i>
    {{ trans('jts.new_import_shipment') }}
  </a>
</li>

