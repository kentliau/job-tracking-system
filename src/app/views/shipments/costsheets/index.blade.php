@extends('layouts.default')

@section('sidebar')
  @include('shipments._edit_sidebar')
@parent
@stop

@section('content')

<div class="panel panel-default">
  <div class="panel-heading clearfix">
    <strong class="panel-title">Costsheets</strong>
    <div class="pull-right">
      <a class="btn btn-primary btn-sm" href="{{ route('shipments.costsheets.create', $shipment->id) }}"><i class="glyphicon glyphicon-plus"></i>{{ trans('jts.new_costsheet') }}</a>
    </div>
  </div>

  <div class="panel-footer"></div>
</div>

@stop