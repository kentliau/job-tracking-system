@extends('layouts.default')

@section('sidebar')
  @include('shipments._edit_sidebar')
@parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong class="panel-title">Items</strong>
        <div class="pull-right">
          <a class="btn btn-primary btn-sm" href="{{ route('shipments.items.create', $shipment->id) }}"><i class="glyphicon glyphicon-plus"></i>{{ trans('jts.new_item') }}</a>
        </div>
      </div>
      <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
          <tr>
            <th style="width:20%;">{{ trans('jts.name') }}</th>
            <th style="width:20%;">{{ trans('jts.serial_number') }}</th>
            <th style="width:60%;">{{ trans('jts.description') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach($items as $item)
              <tr>
                <td><a href="{{ route('shipments.items.edit', [$shipment->id, $item->id]) }}">{{ $item->present()->name }}</a></td>
                <td>{{ $item->serial_number }}</td>
                <td>{{ $item->present()->description }}</td>
              </tr>
            @endforeach
        </tbody>
      </table>

      <div class="panel-footer clearfix">
        <div class="pull-right">
          {{ $items->appends(Input::except('page'))->links() }}
        </div>
      </div>
    </div>

  </div>
</div>
@stop