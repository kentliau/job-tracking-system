<div class="form-group">
  {{ Form::label('name', trans('jts.name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('serial_number', trans('jts.serial_number'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('serial_number', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('serial_number', '<span class="help-block">:message</span>') }}
  </div>
</div>


<div class="form-group">
  {{ Form::label('description', trans('jts.description'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 5] ) }}
    {{ $errors->first('description', '<span class="help-block">:message</span>') }}
  </div>
</div>