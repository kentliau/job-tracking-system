<div class="row">
  <div class="col-md-10">

    <div class="row">

      <div class="col-md-1">
         <strong class="pull-right">{{ trans('jts.type') }} : </strong>
      </div> <!-- .col -->

      <div class="col-md-10">
        <div class="btn-group btn-group-xs">
          <a href="{{ route('shipments.index', Input::except('type') ) }}" class="btn btn-link {{ Request::query('type') ? '' : 'disabled'; }}">{{ trans('ui.all') }}</a>
          <a href="{{ route('shipments.index', array_merge(Input::all(), ['type' => 'export']) ) }}" class="btn btn-link {{ Request::query('type') === 'export' ? 'disabled' : ''; }}"><i class="glyphicon glyphicon-open"></i>{{ trans('jts.export') }}</a>
          <a href="{{ route('shipments.index', array_merge(Input::all(), ['type' => 'import']) ) }}" class="btn btn-link {{ Request::query('type') === 'import' ? 'disabled' : ''; }}"><i class="glyphicon glyphicon-save"></i>{{ trans('jts.import') }}</a>
        </div>
      </div> <!-- .col -->

    </div> <!-- .row -->

    <div class="row">

      <div class="col-md-1">
        <strong class="pull-right">{{ trans('jts.status') }} : </strong>
      </div> <!-- .col -->

      <div class="col-md-10">
          <div class="btn-group btn-group-xs">
            <a href="{{ route('shipments.index', Input::except('status') ) }}" class="btn btn-link {{ Request::query('status') ? '' : 'disabled'; }}">{{ trans('ui.all') }}</a>
            <a href="{{ route('shipments.index', array_merge(Input::all(), ['status' => 'processing']) ) }}" class="dot dot-success btn btn-link {{ Request::query('status') === 'processing' ? 'disabled' : ''; }}">{{ trans('jts.processing') }}</a>
            <a href="{{ route('shipments.index', array_merge(Input::all(), ['status' => 'completed']) ) }}" class="dot dot-primary btn btn-link {{ Request::query('status') === 'completed' ? 'disabled' : ''; }}">{{ trans('jts.completed') }}</a>
            <a href="{{ route('shipments.index', array_merge(Input::all(), ['status' => 'uncompleted']) ) }}" class="dot dot-danger btn btn-link {{ Request::query('status') === 'uncompleted' ? 'disabled' : ''; }}">{{ trans('jts.uncompleted') }}</a>
            <a href="{{ route('shipments.index', array_merge(Input::all(), ['status' => 'cancelled']) ) }}" class="dot dot-default btn btn-link {{ Request::query('status') === 'cancelled' ? 'disabled' : ''; }}">{{ trans('jts.cancelled') }}</a>
          </div>
      </div> <!-- .col -->

    </div> <!-- .row -->

  </div>

</div>