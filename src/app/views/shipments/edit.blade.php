@extends('layouts.default')

@section('sidebar')
  @include('shipments._edit_sidebar')
  @parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="well">

      {{ Form::model($shipment, ['route' => ['shipments.update', $shipment->id], 'method' => 'put', 'class' => 'form-horizontal']) }}
        <fieldset>

          <legend>{{ trans('jts.shipment') }} {{ $shipment->present()->fileNumber }}</legend>

          @include('shipments._form')

          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="pull-right">
                  {{ Form::submit(trans('jts.update_shipment'), ['class' => 'btn btn-primary']) }}
                  {{ Form::button(trans('ui.undo_changes'), ['class' => 'btn btn-default', 'type' => 'reset']) }}
                </div>
                <div class="pull-left">
                  <a class="btn btn-danger" href="{{ route('shipments.destroy', $shipment->id ) }}" data-method="delete">{{ trans('ui.delete') }}</a>
                </div>
            </div>
          </div>

        </fieldset>
      {{ Form::close() }}

    </div> <!-- .well -->
  </div> <!-- .col -->
</div> <!-- .row -->

@stop