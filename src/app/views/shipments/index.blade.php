@extends('layouts.default_simple')

@section('breadcrumb')
@stop

@section('content')

<div class="row">
  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">

        <div class="panel panel-default">
          <div class="panel-heading clearfix">
            <strong class="panel-title">{{ trans('jts.shipments') }}</strong>
            <div class="pull-right">
              <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myModal">
                <i class="glyphicon glyphicon-plus"></i>
                {{ trans('jts.new_job') }}
              </button>
            </div>
          </div>

          <div class="panel-body">
            @include('shipments._table_filter')
          </div>

          <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover table-condensed table-column-resizable" data-resizable-columns-id="shipments-table" >
            <thead>
              <tr>
                <th data-resizable-column-id="file_number">{{ trans('jts.file_number') }}</th>
                <th data-resizable-column-id="status">{{ trans('jts.status') }}</th>
                <th data-resizable-column-id="customer">{{ trans('jts.customer') }}</th>
                <th data-resizable-column-id="description">{{ trans('jts.description') }}</th>
                <th data-resizable-column-id="port">{{ trans('jts.port') }}</th>
                <th data-resizable-column-id="vessel">{{ trans('jts.vessel') }}</th>
                <th data-resizable-column-id="estimated_time_arrival" title="{{ Input::get('type') === 'import' ?
                                trans('jts.estimated_time_arrival') :
                                (Input::get('type') === 'export' ?
                                  trans('jts.estimated_time_deliver') :
                                  trans('jts.estimated_time_arrival') . '/' . trans('jts.estimated_time_deliver')) }}">
                  {{ Input::get('type') === 'import' ?
                      trans('jts.estimated_time_arrival_abbr') :
                      (Input::get('type') === 'export' ?
                        trans('jts.estimated_time_deliver_abbr') :
                        trans('jts.estimated_time_arrival_abbr') . '/' . trans('jts.estimated_time_deliver_abbr')) }}
                </th>
                <th data-resizable-column-id="bill_of_lading_number" title="{{ trans('jts.bill_of_lading_number') }}">{{ trans('jts.bill_of_lading_number_abbr') }}</th>
              </tr>
            </thead>
            <tbody>

            @foreach( $shipments as $shipment )
              <tr>
                <td>
                  <a href="{{ route('shipments.edit', $shipment->id) }}">
                    {{{ $shipment->present()->fileNumber }}}
                  </a>
                </td>

                <td>
                  @include('shipments._update_status')
                </td>

                <td>
                  {{-- $shipment['shipper']['id'] --}}
                  <strong>{{ trans('jts.shipper') }}:</strong>
                  {{{ $shipment->present()->shipper->present()->name }}}
                  <br>
                  <strong>{{ trans('jts.consignee') }}:</strong>
                  {{{ $shipment->present()->consignee->present()->name }}}
                </td>

                <td>{{{ $shipment->present()->description }}}</td>

                <td>
                  <strong>{{ trans('jts.load_port') }}:</strong>
                  {{{ $shipment->present()->loadPort->present()->name }}}
                  <br>
                  <strong>{{ trans('jts.discharge_port') }}:</strong>
                  {{{ $shipment->present()->dischargePort->present()->name }}}
                </td>

                <td>
                  {{{ $shipment->present()->vessel->present()->name }}}
                  <br>
                  <strong>{{ trans('jts.vessel_lot') }}:</strong>
                  {{{ $shipment->present()->vesselLot }}}
                </td>

                <td>
                  <time datetime="{{{ $shipment->estimated_arrival_date }}} {{{ 'GMT+0800' }}}">
                    {{{ $shipment->present()->estimatedArrivalDate }}}
                  </time>
                </td>

                <td>{{{ $shipment->present()->billOfLadingNumber }}}</td>
              </tr>
            @endforeach

            </tbody>
          </table>
          </div>
          <div class="panel-footer clearfix">
            <div class="pull-right">
              {{ $shipments->appends(Input::except('page'))->links() }}
            </div>
          </div>
        </div>

  </div> <!-- .col -->
</div> <!-- .row -->

@include('shipments._modal')

@stop


