<!-- Modal -->
<div class="modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">{{ trans('jts.select_type_of_job') }}</h4>
      </div>
      <div class="modal-body">

        <div class="row">
          <div class="col-md-12">
              <div class="btn-group btn-group-justified" role="group" aria-label="...">

                <a class="btn btn-default btn-square" style="padding: 40px;" href="{{ route('shipments.create', ['type' => 'export'])  }}">
                  <i class="glyphicon glyphicon-open" style="font-size: 10em;"></i><br/>{{ trans('jts.new_export_shipment') }}
                </a>

                <a class="btn btn-default btn-square" style="padding: 40px;" href="{{ route('shipments.create', ['type' => 'import'])  }}">
                  <i class="glyphicon glyphicon-save" style="font-size: 10em;"></i><br/>{{ trans('jts.new_import_shipment') }}
                </a>

              </div>
          </div>
        </div>

      </div>

    </div>
  </div>
</div>
