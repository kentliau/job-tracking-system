@if ($shipment->id)

  <div class="form-group">
    {{ Form::label('file_date', trans('jts.file_date'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-4">
      <p class="form-control-static">{{ $shipment->present()->fileDate }}</p>
    </div>
  </div>

@else

  <div class="form-group">
    {{ Form::label('type', trans('jts.type'), ['class' => 'col-md-3 control-label'] ) }}
    <div class="col-md-9">
      <p class="form-control-static text-capitalize">{{ Input::old('type', isset($shipment) ? $shipment->type : null) }}</p>
      {{ Form::hidden('type', Input::old('type', isset($shipment) ? $shipment->type : null)) }}
    </div>
  </div>

  <div class="form-group {{ $errors->has('file_number') ? 'has-error has-feedback' : ''; }}">
    {{ Form::label('file_number', trans('jts.file_number'), ['class' => 'col-md-3 control-label'] ) }}
    <div class="col-md-9">
      {{ Form::number('file_number', null, ['class' => 'form-control'] ) }}
      @if ($errors->has('file_number'))
        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
      @endif
      {{ $errors->first('file_number', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group {{ $errors->has('file_number') ? 'has-error has-feedback' : ''; }}">
    {{ Form::label('file_date', trans('jts.file_date'), ['class' => 'col-md-3 control-label'] ) }}
    <div class="col-md-9">

      {{ Form::text('file_date_display', Input::old('file_date_display', isset($shipment) ? $shipment->present()->fileDate : null), ['class' => 'form-control datetime-control', 'data-date-format' => 'DD/MM/YYYY', 'data-mirror' => '#file_date'] ) }}
      {{ Form::hidden('file_date') }}

      @if ($errors->has('file_number'))
        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
      @endif
      {{ $errors->first('file_date', '<span class="help-block">:message</span>') }}
      {{ $errors->first('file_number', '<span class="help-block">:message</span>') }}
    </div>
  </div>

@endif

@if ($shipment->id)

  <div class="form-group">
    {{ Form::label('status', trans('jts.status'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::select('status',
          \JTS\App::statusSelection(),
          null,
          ['class' => 'form-control']) }}
      {{ $errors->first('status', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('shipper_name', trans('jts.shipper_name'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::select('shipper_id', [$shipment->shipper_id => $shipment->shipper['name'] ], null, ['class' => 'form-control selectize', 'data-data' => 'customers'] ) }}
      {{ $errors->first('shipper_id', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('consignee_name', trans('jts.consignee_name'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::select('consignee_id', [$shipment->consignee_id => $shipment->consignee['name'] ],null, ['class' => 'form-control selectize', 'data-data' => 'customers'] ) }}
      {{ $errors->first('consignee_id', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('description', trans('jts.description'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 5] ) }}
      {{ $errors->first('description', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('load_port_name', trans('jts.load_port_name'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::select('load_port_id', [$shipment->load_port_id => $shipment->loadPort['name']], null, ['class' => 'form-control selectize', 'data-data' => 'ports'] ) }}
      {{ $errors->first('load_port_id', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('discharge_port_name', trans('jts.discharge_port_name'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::select('discharge_port_id', [$shipment->discharge_port_id => $shipment->dischargePort['name']], null, ['class' => 'form-control selectize', 'data-data' => 'ports'] ) }}
      {{ $errors->first('discharge_port_id', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('vessel_name', trans('jts.vessel_name'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::select('vessel_id', [$shipment->vessel_id => $shipment->vessel['name']],null, ['class' => 'form-control selectize', 'data-data' => 'vessels'] ) }}
      {{ $errors->first('vessel_id', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('vessel_lot', trans('jts.vessel_lot'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::text('vessel_lot', null, ['class' => 'form-control'] ) }}
      {{ $errors->first('vessel_lot', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('estimated_arrival_date', trans('jts.estimated_arrival_date'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::text('estimated_arrival_date_display', Input::old('estimated_arrival_date_display', isset($shipment) ? $shipment->present()->estimatedArrivalDate : null), ['class' => 'form-control datetime-control', 'data-date-format' => 'DD/MM/YYYY hh:mm A', 'data-mirror' => '#estimated_arrival_date'] ) }}
      {{ Form::hidden('estimated_arrival_date') }}

      {{ $errors->first('estimated_arrival_date', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('bill_of_lading_number', trans('jts.bill_of_lading_number'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::text('bill_of_lading_number', null, ['class' => 'form-control'] ) }}
      {{ $errors->first('bill_of_lading_number', '<span class="help-block">:message</span>') }}
    </div>
  </div>

  <div class="form-group">
    {{ Form::label('assigned_to', trans('jts.assigned_to'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      <select name="assigned_to[]" id="assigned_to" multiple="multiple" class="form-control selectize-multiple">
        @foreach($userSelectionListing as $user)
          <option {{ in_array($user->id, $shipment->users->lists('id')) ? 'selected': '' }} value="{{ $user->id }}">{{ $user->present()->identifier }}</option>
        @endforeach
      </select>
      {{ $errors->first('creator', '<span class="help-block">:message</span>') }}
    </div>
  </div>

@endif

