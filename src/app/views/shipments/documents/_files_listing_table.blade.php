@if( isset($files) )
<div class="form-group">
  {{ Form::label('current_files', trans('jts.current_files'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    <table class="table table-hover table-condensed table-bordered">
        <tbody>
        @foreach($files as $file)
          <tr>
            <td>
              {{ $file->getRelativePathname() }}
            </td>

            <td>
              <a href="{{ '#'  }}" class="btn btn-xs btn-link disabled" data-method="get">{{ trans('ui.preview') }}</a>
              <a href="{{ '#'  }}" class="btn btn-xs btn-link disabled" data-method="get">{{ trans('ui.download') }}</a>
              <a href="{{ '#'  }}" class="btn btn-xs btn-link disabled" data-method="delete">{{ trans('ui.delete') }}</a>
            </td>
          </tr>
        @endforeach
        </tbody>
    </table>

    <a href="{{ route('shipments.documents.show', [$shipment->id, $document->id])  }}?download=1">{{ trans('ui.download_all') }}  ({{ trans('ui.in_zip') }})</a>

  </div>
</div>
@endif