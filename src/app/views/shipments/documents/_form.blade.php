<div class="form-group">
  {{ Form::label('document_id', trans('jts.document_type'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::select('document_id', $documentListing, null, ['class' => 'form-control'] ) }}
    {{ $errors->first('document_id', '<span class="help-block">:message</span>') }}
  </div>
</div>

{{--
<div class="form-group">
  {{ Form::label('name', trans('jts.name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
  </div>
</div>
--}}

<div class="form-group">
  {{ Form::label('serial_number', trans('jts.serial_number'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('serial_number', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('serial_number', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('files', trans('jts.files'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::file('files[]', ['class' => 'filestyle', 'multiple' => true, 'data-badge' => true] ) }}
    {{ $errors->first('files', '<span class="help-block">:message</span>') }}
  </div>
</div>

@include('shipments.documents._files_listing_table')