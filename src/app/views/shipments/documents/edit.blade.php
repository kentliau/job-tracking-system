@extends('layouts.default')

@section('sidebar')
  @include('shipments._edit_sidebar')
  @parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="well">

      {{ Form::model($document, ['files' => true, 'route' => ['shipments.documents.update', $shipment->id, $document->id], 'method' => 'put', 'class' => 'form-horizontal']) }}

        <fieldset>
          <legend>{{ trans('jts.editing_document', ['fileNumber' => $fileNumber]) }}</legend>

          @include('shipments.documents._form')

          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="pull-right">
                  {{ Form::submit(trans('ui.update'), ['class' => 'btn btn-primary']) }}
                  {{ Form::button(trans('ui.undo_changes'), ['class' => 'btn btn-default', 'type' => 'reset']) }}
                </div>
                <div class="pull-left">
                  <a class="btn btn-danger" href="{{ route('shipments.documents.destroy', [$shipment->id, $document->id] ) }}" data-method="delete">{{ trans('ui.delete') }}</a>
                </div>
            </div>
          </div>
        </fieldset>

      {{ Form::close() }}

    </div> <!-- .well -->
  </div> <!-- .col -->
</div> <!-- .row -->

@stop