@extends('layouts.default')

@section('sidebar')
  @include('shipments._edit_sidebar')
@parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong class="panel-title">Documents</strong>
        <div class="pull-right">
          <a class="btn btn-primary btn-sm" href="{{ route('shipments.documents.create', $shipment->id) }}"><i class="glyphicon glyphicon-plus"></i>{{ trans('jts.new_document') }}</a>
        </div>
      </div>

      <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
          <tr>
            <th style="width:20%;">{{ trans('jts.serial_number') }}</th>
            <th style="width:20%;">{{ trans('jts.document_type') }}</th>
            {{-- <th style="width:20%;">{{ trans('jts.name') }}</th> --}}

          </tr>
        </thead>
        <tbody>
            @foreach($documents as $document)
              <tr>
                <td><a href="{{ route('shipments.documents.edit', [$shipment->id, $document->id]) }}">{{ $document->serial_number }}</a></td>
                <td>{{ $document->document->name }}</td>
                {{-- <td>{{ $document->present()->name }}</td> --}}
              </tr>
            @endforeach
        </tbody>
      </table>

      <div class="panel-footer clearfix">
        <div class="pull-right">
          {{ $documents->appends(Input::except('page'))->links() }}
        </div>
      </div>
    </div>

  </div>
</div>
@stop