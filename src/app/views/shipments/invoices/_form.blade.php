<div class="form-group">
  {{ Form::label('invoice_number', trans('jts.invoice_number'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('invoice_number', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('invoice_number', '<span class="help-block">:message</span>') }}
  </div>
</div>


<div class="form-group">
  {{ Form::label('description', trans('jts.description'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::textarea('description', null, ['class' => 'form-control', 'rows' => 5] ) }}
    {{ $errors->first('description', '<span class="help-block">:message</span>') }}
  </div>
</div>