@extends('layouts.default')

@section('sidebar')
  @include('shipments._edit_sidebar')
@parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong class="panel-title">Invoices</strong>
        <div class="pull-right">
          <a class="btn btn-primary btn-sm" href="{{ route('shipments.invoices.create', $shipment->id) }}"><i class="glyphicon glyphicon-plus"></i>{{ trans('jts.new_invoice') }}</a>
        </div>
      </div>

      <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
          <tr>
            <th width="10%">{{ trans('jts.invoice_number') }}</th>
            <th>{{ trans('jts.description') }}</th>
            <th width="20%">{{ trans('ui.action') }}</th>
          </tr>
        </thead>
        <tbody>
            @foreach($invoices as $invoice)
              <tr>
                <td><a href="{{ route('shipments.invoices.edit', [$shipment->id, $invoice->id]) }}">{{ $invoice->present()->invoiceNumber }}</a></td>
                <td>{{ $invoice->present()->description }}</td>
                <td><a href="{{ route('shipments.invoices.show', [$shipment->id, $invoice->id]) }}?pdf=1" target="_window"><i class="glyphicon glyphicon-print"></i>Generate PDF</a></td>
              </tr>
            @endforeach
        </tbody>
      </table>

      <div class="panel-footer clearfix">
        <div class="pull-right">
          {{ $invoices->appends(Input::except('page'))->links() }}
        </div>
      </div>
    </div>

  </div>
</div>
@stop