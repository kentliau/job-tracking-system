<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=yes">
  <title>Print Friendly Invoice</title>
  <link rel="stylesheet" href="{{ asset('assets/bower/bootstrap/dist/css/bootstrap.min.css') }}">
  <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:100' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Ubuntu+Mono' rel='stylesheet' type='text/css'>
  <style>
  /*******************************************************************
  This chunk is to fix Bootstrap so that the Markdown output looks good
  *******************************************************************/
  xmp, textarea {
    display: none;
  }

  h1,h2,h3,h4 {
    margin: 15px 0;
  }

  pre {
    margin: 20px 0;
  }

  img {
    margin: 10px 0;
  }


  /*******************************************************************
  This chunk is for Google's Code Prettify:
  http://google-code-prettify.googlecode.com
  *******************************************************************/
  /* Pretty printing styles. Used with prettify.js. */
  /* SPAN elements with the classes below are added by prettyprint. */
  .pln { color: #000 }  /* plain text */

  @media screen {
    .str { color: #080 }  /* string content */
    .kwd { color: #008 }  /* a keyword */
    .com { color: #800 }  /* a comment */
    .typ { color: #606 }  /* a type name */
    .lit { color: #066 }  /* a literal value */
    /* punctuation, lisp open bracket, lisp close bracket */
    .pun, .opn, .clo { color: #660 }
    .tag { color: #008 }  /* a markup tag name */
    .atn { color: #606 }  /* a markup attribute name */
    .atv { color: #080 }  /* a markup attribute value */
    .dec, .var { color: #606 }  /* a declaration; a variable name */
    .fun { color: red }  /* a function name */
  }

  /* Use higher contrast and text-weight for printable form. */
  @media print, projection {
    .str { color: #060 }
    .kwd { color: #006; font-weight: bold }
    .com { color: #600; font-style: italic }
    .typ { color: #404; font-weight: bold }
    .lit { color: #044 }
    .pun, .opn, .clo { color: #440 }
    .tag { color: #006; font-weight: bold }
    .atn { color: #404 }
    .atv { color: #060 }
  }

  /* Put a border around prettyprinted code snippets. */
  pre.prettyprint { padding: 2px; border: 1px solid #888 }

  /* Specify class=linenums on a pre to get line numbering */
  ol.linenums { margin-top: 0; margin-bottom: 0 } /* IE indents via margin-left */
  li.L0,
  li.L1,
  li.L2,
  li.L3,
  li.L5,
  li.L6,
  li.L7,
  li.L8 { list-style-type: none }
  /* Alternate shading for lines */
  li.L1,
  li.L3,
  li.L5,
  li.L7,
  li.L9 { background: #eee }


  html{font-size:100%;-webkit-text-size-adjust:100%;-ms-text-size-adjust:100%;}
  a:focus{outline:thin dotted #333;outline:5px auto -webkit-focus-ring-color;outline-offset:-2px;}
  a:hover,a:active{outline:0;}
  sub,sup{position:relative;font-size:75%;line-height:0;vertical-align:baseline;}
  sup{top:-0.5em;}
  sub{bottom:-0.25em;}
  img{max-width:100%;width:auto\9;height:auto;vertical-align:middle;border:0;-ms-interpolation-mode:bicubic;}
  button,input,select,textarea{margin:0;font-size:100%;vertical-align:middle;}
  button,input{*overflow:visible;line-height:normal;}
  button::-moz-focus-inner,input::-moz-focus-inner{padding:0;border:0;}
  button,input[type="button"],input[type="reset"],input[type="submit"]{cursor:pointer;-webkit-appearance:button;}
  input[type="search"]{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;-webkit-appearance:textfield;}
  input[type="search"]::-webkit-search-decoration,input[type="search"]::-webkit-search-cancel-button{-webkit-appearance:none;}
  textarea{overflow:auto;vertical-align:top;}
  body{margin:0;font-family: "Helvetica Neue",Helvetica,sans-serif;font-size:15px;line-height:25px;color:#333333;background-color:#ffffff;}
  a{color:#dd4814;text-decoration:none;font-family:'Ubuntu Mono';}
  a:hover{color:#97310e;text-decoration:underline;}
  p{margin:0 0 10px;}
  small{font-size:85%;}
  strong{font-weight:bold;}
  em{font-style:italic;}
  cite{font-style:normal;}

  h1,h2,h3,h4,h5,h6{margin:10px 0;font-family:inherit;font-weight:normal;line-height:1;color:inherit;text-rendering:optimizelegibility;}h1 small,h2 small,h3 small,h4 small,h5 small,h6 small{font-weight:normal;line-height:1;color:#999999;}
  h1{font-size:36px;line-height:40px;}
  h2{font-size:30px;line-height:40px;}
  h3{font-size:24px;line-height:40px;}
  h4{font-size:18px;line-height:20px;}
  h5{font-size:14px;line-height:20px;}
  h6{font-size:12px;line-height:20px;}
  h1 small{font-size:24px;}
  h2 small{font-size:18px;}
  h3 small{font-size:14px;}
  h4 small{font-size:14px;}

  ul,ol{padding:0;margin:0 0 10px 25px;}
  ul ul,ul ol,ol ol,ol ul{margin-bottom:0;}
  li{line-height:20px;}
  ul.unstyled,ol.unstyled{margin-left:0;list-style:none;}
  dl{margin-bottom:20px;}
  dt,dd{line-height:20px;}
  dt{font-weight:bold;}
  dd{margin-left:10px;}
  hr{margin:20px 0;border:0;border-top:1px solid #f5f5f5;border-bottom:1px solid #ffffff;}
  abbr[title]{cursor:help;border-bottom:1px dotted #999999;}
  abbr.initialism{font-size:90%;text-transform:uppercase;}
  blockquote{padding:0 0 0 15px;margin:0 0 20px;border-left:5px solid #f5f5f5;}blockquote p{margin-bottom:0;font-size:16px;font-weight:300;line-height:25px;}
  blockquote small{display:block;line-height:20px;color:#999999;}blockquote small:before{content:'\2014 \00A0';}
  blockquote.pull-right{float:right;padding-right:15px;padding-left:0;border-right:5px solid #f5f5f5;border-left:0;}blockquote.pull-right p,blockquote.pull-right small{text-align:right;}
  blockquote.pull-right small:before{content:'';}
  blockquote.pull-right small:after{content:'\00A0 \2014';}
  q:before,q:after,blockquote:before,blockquote:after{content:"";}
  address{display:block;margin-bottom:20px;font-style:normal;line-height:20px;}
  code,pre{padding:0 3px 2px;font-family:Menlo,Monaco,Consolas,"Courier New",monospace;font-size:12px;color:#333333;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;}
  code{padding:2px 4px;color:#d14;background-color:#f7f7f9;border:1px solid #e1e1e8;}
  pre{display:block;padding:9.5px;margin:0 0 10px;font-size:13px;line-height:20px;word-break:break-all;word-wrap:break-word;white-space:pre;white-space:pre-wrap;background-color:#f5f5f5;border:1px solid #ccc;border:1px solid rgba(0, 0, 0, 0.15);-webkit-border-radius:4px;-moz-border-radius:4px;border-radius:4px;}pre.prettyprint{margin-bottom:20px;}
  pre code{padding:0;color:inherit;background-color:transparent;border:0;}


  body {
    background-color: #9BA2B0;
  }

  sup {
    font-size: .25em;
    top: -1.5em;
  }
  .container {
    background-color: white;
    width: 1000px;
    margin: 10px auto;
    padding: 30px 20px;
    box-shadow: 0 0 10px 0 rgba(0,0,0,.5);
  }

  hr {
    border-top: 1px solid black;
    border-bottom: 1px solid black;
  }

  .name {
    font-family: 'Roboto Slab', serif;
    font-size: 4em;
    font-weight: 100;
    text-align: center;
    line-height: 60px;
  }

  @media (max-width: 768px){
    .container {
      width: 85%;
      margin: 10px auto;
      padding: 10px 20px;
    }
  }

  /* Use higher contrast and text-weight for printable form. */
  @media print, projection {
       body {
        background-color: white;
      }

      .container {
      background-color: white;
      width: 100%;
      margin: auto auto;
      padding: 0;
      box-shadow: 0;
    }

  }
  </style>
</head>
<body>
  <div class="container">

    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div style="background-color:red;width:150px;height:150px;"></div>
      </div> <!-- .col -->
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div style="background-color:red;width:100%;height:50px;color:white;font-size:25px;line-height: 50px;text-align: center;">TENAGA TIMUR FREIGHT (M) SDN BHD</div><br>
          TENAGA TIMUR FREIGHT (M) SDN. BHD.<br>
          (COMPANY NO.: 131190-T)<br>
          10B, LORONG 8/1E, 46050 PETALING JAYA,<br>
          SELANGOR DARUL EHSAN, MALAYSIA.<br>
          TEL: 03-79559433  FAX: 03-79578580<br>
          E-MAIL: ttfrtmy@gmail.com
      </div> <!-- .col -->
    </div> <!-- .row -->


    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div style="border-bottom:2px solid black;"></div>
        <div class="pull-left" contenteditable>
          TENAGA TIMUR FREIGHT (M) SDN. BHD.<br>
          (COMPANY NO.: 131190-T)<br>
          10B, LORONG 8/1E, 46050 PETALING JAYA,<br>
          SELANGOR DARUL EHSAN, MALAYSIA.<br>
          TEL: 03-79559433  FAX: 03-79578580<br>
          E-MAIL: ttfrtmy@gmail.com
        </div>
        <div class="pull-right">
        <div class="" style="font-size:30px;background:black;color:white;padding:10px;">INVOICE</div>
        <div class="" style="font-size:30px;padding:30px 0 0 0;text-align:center;">{{ $invoice->present()->invoiceNumber }}</div>
        </div>

      </div> <!-- .col -->
    </div> <!-- .row -->

    <br>


    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <strong>Job No. :</strong> ( {{ $shipment->present()->fileNumber }} )
      </div> <!-- .col -->
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <div>
          <span class="pull-right clearfix"><strong>Date: </strong> {{ $shipment->present()->fileDate }} </span>
        </div>
      </div> <!-- .col -->
    </div> <!-- .row -->


    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <table class="table table-bordered table-condensed">
          <thead>
            <tr>
              <th class="text-center">ITEM</th>
              <th class="text-center">PARTICULARS</th>
              <th class="text-center">AMOUNT</th>
            </tr>
          </thead>

          <tbody>
            <tr>
              <td></td>
              <td contenteditable>
                <div>SUBJECT : </div>
                <div>VESSEL : </div>
                <div>FROM/TO : </div>
                <div>B/L NO : </div>
              </td>
              <td></td>
            </tr>
            @for($i = 0; $i < 12; $i++)
            <tr>
              <td contenteditable>&nbsp</td>
              <td contenteditable>&nbsp</td>
              <td contenteditable>&nbsp</td>
            </tr>
            @endfor
          </tbody>

          <tfoot>
            <tr>
              <th></th>
              <th class="text-right">Total</th>
              <th contenteditable></th>
            </tr>
          </tfoot>

        </table>
      </div> <!-- .col -->
    </div> <!-- .row -->


    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">

      </div> <!-- .col -->

      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        <br>
        <br>
        <br>
        <br>
        <hr>
        <div class="text-center">TENAGA TIMUR FREIGHT (M) SDN. BHD.</div>
      </div> <!-- .col -->
    </div> <!-- .row -->


  </div>
</body>
</html>