@extends('layouts.default')

@section('sidebar')
  @include('shipments_invoices_items._index_sidebar')
@parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <table class="table table-striped table-bordered table-hover table-condensed">
      <thead>
        <tr>
          <th style="width:30%;">{{ trans('jts.description') }}</th>
          <th>{{ trans('jts.amount') }}</th>
        </tr>
      </thead>
      <tbody>
          @foreach($items as $item)
            <tr>
              <td>{{ $item->present()->description }}</td>
              <td>{{ $item->amount }}</td>
            </tr>
          @endforeach
      </tbody>
    </table>

    <div class="pull-right">
      {{ $items->appends(Input::except('page'))->links() }}
    </div>

  </div>
</div>
@stop