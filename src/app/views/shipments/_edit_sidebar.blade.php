<li class="{{ Request::is('shipments/*/edit') ? 'active' : '' }}">
  <a href="{{ route('shipments.edit', $shipment->id) }}">
  <i class="glyphicon glyphicon-plane"></i>
  {{ trans('jts.shipment') }} {{ $fileNumber }}
  </a>
</li>

<li class="{{ Request::is('shipments/*/items') ? 'active' : '' }}">
  <a href="{{ route('shipments.items.index', $shipment->id) }}">
  <i class="glyphicon glyphicon-list"></i>
  {{ trans('jts.items') }}
  </a>
</li>

<li class="disabled {{ Request::is('shipments/*/tasks') ? 'active' : '' }}">
  <a href="{{ route('shipments.tasks.index', $shipment->id) }}">
    <i class="glyphicon glyphicon-tasks"></i>
    {{ trans('jts.tasks') }}
  </a>
</li>

<li class="{{ Request::is('shipments/*/documents') ? 'active' : '' }}">
  <a href="{{ route('shipments.documents.index', $shipment->id) }}">
    <i class="glyphicon glyphicon-folder-open"></i>
    {{ trans('jts.documents') }}
  </a>
</li>

<li class="disabled {{ Request::is('shipments/*/costsheets') ? 'active' : '' }}">
  <a href="{{ route('shipments.costsheets.index', $shipment->id) }}">
    <i class="glyphicon glyphicon-usd"></i>
    {{ trans('jts.costsheets') }}
  </a>
</li>

<li class="{{ Request::is('shipments/*/invoices') ? 'active' : '' }}">
  <a href="{{ route('shipments.invoices.index', $shipment->id) }}">
    <i class="glyphicon glyphicon-list-alt"></i>
    {{ trans('jts.invoices') }}
  </a>
</li>

