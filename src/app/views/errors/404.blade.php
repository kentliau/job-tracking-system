@extends('layouts.default_simple')

@section('content')
<div class="row">
  <div class="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-xs-6 col-sm-6 col-md-6 col-lg-6">
    <h4>It seem like something you looking for is missing.</h4>
    <p>
    Don't Worry. We had dispatched a squad of highly trained monkeys to deal with this situation. <br>
    If you see them, tell them your what you are looking for. <br>
    Or, drop them an email at <a href="">monkey@jts.app</a>
    </p>
  </div> <!-- .col -->
</div> <!-- .row -->
@stop