<div class="form-group">
  {{ Form::label('email', trans('ui.email'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('email', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('email', '<span class="help-block">:message</span>') }}
  </div>
</div>

@if( !isset($user) or Input::has('changePassword'))
  <div class="form-group">
    {{ Form::label('password', trans('ui.password'), ['class' => 'col-md-2  control-label'] ) }}
    <div class="col-md-10">
      {{ Form::text('password', '', ['class' => 'form-control'] ) }}
      {{ $errors->first('password', '<span class="help-block">:message</span>') }}
    </div>
  </div>
@endif

<div class="form-group">
  {{ Form::label('first_name', trans('ui.first_name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('first_name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('first_name', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('last_name', trans('ui.last_name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('last_name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('last_name', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('group', trans('ui.group'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::select('group_id',
        $groupSelectionListing,
        Input::old('group_id', isset($user) ? $user->groups->first()->id : null),
        ['class' => 'form-control']) }}
    {{ $errors->first('group', '<span class="help-block">:message</span>') }}
  </div>
</div>