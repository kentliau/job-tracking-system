@extends('layouts.default')

@section('sidebar')
  @include('hrm._sidebar')
@stop

@section('breadcrumb')
@stop

@section('content')
  @include('hrm.users._users_table')
@stop