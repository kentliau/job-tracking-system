<div class="panel panel-default">
  <div class="panel-heading clearfix">
    <strong class="panel-title">{{ trans('jts.users') }}</strong>
    <div class="pull-right">
      <a href="{{ route('users.create') }}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> {{ trans('jts.new_user') }}</a>
    </div>
  </div>

  <table class="table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th>Employee Full Name</th>
      <th>Roles</th>
      <th>Last Login</th>
      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

    @foreach($users as $user)
    <tr>
      <td><a href="{{ route('users.edit', $user->id) }}">{{ $user->present()->identifier }}</a></td>
      <td>{{ $user->present()->group }}</td>
      <td>{{ $user->last_login }}</td>
      <td>
        {{--<a href="#" class="btn btn-default btn-xs">Statistic</a>--}}
        <a href="{{ route('users.edit', $user->id) }}?changePassword=1" class="btn btn-default btn-xs">Change password</a>
        <a href="{{ route('users.edit', $user->id) }}" class="btn btn-default btn-xs">Edit</a>
        {{-- <a href="#" class="btn btn-default btn-xs">Ban</a> --}}
      </td>
    </tr>
    @endforeach


  </tbody>
  </table>

  <div class="panel-footer clearfix">
    <div class="pull-right">
      {{ $users->appends(Input::except('page'))->links() }}
    </div>
  </div>
</div>