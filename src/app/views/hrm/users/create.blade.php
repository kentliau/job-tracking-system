@extends('layouts.default')

@section('sidebar')
  @include('hrm._sidebar')
  @parent
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="well">

      {{ Form::open(['route' => 'users.store', 'method' => 'post', 'class' => 'form-horizontal']) }}

        <fieldset>
          <legend>{{ trans('jts.creating_new_user') }}</legend>

          @include('hrm.users._form')

          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="pull-right">
                {{ Form::submit(trans('ui.save'), ['class' => 'btn btn-primary']) }}
              </div>
            </div>
          </div>
        </fieldset>

      {{ Form::close() }}

    </div> <!-- .well -->
  </div> <!-- .col -->
</div> <!-- .row -->
@stop
