<div class="panel panel-default">
  <div class="panel-heading clearfix">
    <strong class="panel-title">{{ trans('jts.groups') }}</strong>
    <div class="pull-right">
      <a href="#" class="disabled btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i>{{ trans('jts.new_group') }}</a>
    </div>
  </div>

  <table class="table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th>Group name</th>
      <th>Permissions</th>
      <th>Number of users</th>
    </tr>
  </thead>
  <tbody>
    <tr>
        <td>Superuser</td>
        <td>Everything (It bypass any permission requirement).</td>
        <td>1</td>

    </tr>
    <tr>

      <td>Administrator</td>
      <td>Can read, write, and delete everyone's things. Can manage human resources.</td>
      <td>3</td>

    </tr>
    <tr>

      <td>Operators</td>
      <td>Can read, write, and delete involved things</td>
      <td>1</td>

    </tr>

  </tbody>
  </table>
  <div class="panel-footer"></div>
</div>
