<li class="{{ Request::is('users') ? 'active' : '' }}">
  <a href="{{ route('users.index') }}">
  <i class="glyphicon glyphicon-user"></i>
  {{ trans('jts.users') }}
  </a>
</li>

<li class="{{ Request::is('groups') ? 'active' : '' }}">
  <a href="{{ route('groups.index') }}">
  <i class="glyphicon glyphicon-check"></i>
  {{ trans('jts.groups') }}
  </a>
</li>

