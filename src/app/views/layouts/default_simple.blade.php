@extends('layouts.default')

@section('container')
    <div class="container-fluid">
      <div class="row">

        <div class="col-md-12 wrapper">

          @include('partials._breadcrumb')
          @include('partials._notifications')
          @include('partials._errors')

          @yield('content')

        </div>
      </div>
    </div>
@stop