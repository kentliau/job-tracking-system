<!DOCTYPE html>
<!--[if IE 8]> <html class="no-js lt-ie9" lang="en" > <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en" > <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>JTS</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">

  @section('styles')
    <!-- Third Party -->
    <link rel="stylesheet" href="{{ asset('assets/bower/bootswatch/paper/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower/jquery-resizable-columns/dist/jquery.resizableColumns.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/bower/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" >
    <link rel="stylesheet" href="{{ asset('assets/bower/c3/c3.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bower/animate.css/animate.min.css') }}">

    <!-- Custom styles -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.css') }}">

    {{-- Use @section for page specific styles --}}
  @show

  {{-- pre-required scripts for all pages here --}}

</head>
<body>

  <main>

    @include('partials._top_navigation')

    @section('container')
    <div class="container-fluid">
      <div class="row">

        <div class="col-md-2">
          @include('partials._sidebar')
        </div>

        <div class="col-md-10 wrapper">

          @include('partials._breadcrumb')

          @include('partials._notifications')
          @include('partials._errors')

          @yield('content')

        </div>
      </div>
    </div>
    @show

  </main>

  @include('partials._footer')

  @section('scripts')
    {{-- scripts for all pages here --}}
    <script src="{{ asset('assets/bower/angularjs/angular.min.js') }}"></script>
    <script src="{{ asset('assets/bower/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/bower/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <script src="{{ asset('assets/bower/store.js/store.min.js') }}"></script>
    {{--<script src="{{ asset('assets/bower/store.js/store+json2.min.js') }}"></script>--}}
    <script src="{{ asset('assets/bower/jquery-resizable-columns/dist/jquery.resizableColumns.min.js') }}"></script>
    <script src="{{ asset('assets/bower/moment/min/moment-with-locales.min.js') }}"></script>
    <script src="{{ asset('assets/bower/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="{{ asset('assets/bower/d3/d3.min.js') }}"></script>
    <script src="{{ asset('assets/bower/c3/c3.min.js') }}"></script>

    <script src="{{ asset('assets/bower/bootstrap-filestyle/src/bootstrap-filestyle.js') }}"></script>

    <script src="{{ asset('assets/bower/selectize/dist/js/standalone/selectize.js') }}"></script>

    <script src="{{ asset('assets/js/app.js') }}"></script>
    {{-- Use @section for page specific scripts --}}
  @show
</body>
</html>