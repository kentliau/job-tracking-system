@extends('layouts.default')

@section('sidebar')
  @include('resources._sidebar')
@stop

@section('breadcrumb')
@stop

@section('content')
@extends('layouts.default')

@section('sidebar')
  @include('resources._sidebar')
@stop

@section('breadcrumb')
@stop

@section('content')
<div class="panel panel-default">
  <div class="panel-heading clearfix">
    <strong class="panel-title">{{ trans('jts.ports') }}</strong>
    <div class="pull-right">
      <a href="{{ route('ports.create') }}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> {{ trans('jts.new_port') }}</a>
    </div>
  </div>

  <table class="table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th>Name</th>

      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

    @foreach($ports as $port)
    <tr>
      <td><a href="{{ route('ports.edit', $port->id) }}">{{ $port->present()->name }}</a></td>
      <td>
        <a href="{{ route('ports.edit', $port->id) }}" class="btn btn-default btn-xs">Edit</a>
      </td>
    </tr>
    @endforeach


  </tbody>
  </table>

  <div class="panel-footer clearfix">
    <div class="pull-right">
      {{ $ports->appends(Input::except('page'))->links() }}
    </div>
  </div>
</div>
@stop
@stop