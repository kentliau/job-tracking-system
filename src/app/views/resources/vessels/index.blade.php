@extends('layouts.default')

@section('sidebar')
  @include('resources._sidebar')
@stop

@section('breadcrumb')
@stop

@section('content')
<div class="panel panel-default">
  <div class="panel-heading clearfix">
    <strong class="panel-title">{{ trans('jts.vessels') }}</strong>
    <div class="pull-right">
      <a href="{{ route('vessels.create') }}" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> {{ trans('jts.new_vessel') }}</a>
    </div>
  </div>

  <table class="table table-bordered table-striped table-hover">
  <thead>
    <tr>
      <th>Name</th>

      <th>Actions</th>
    </tr>
  </thead>
  <tbody>

    @foreach($vessels as $vessel)
    <tr>
      <td><a href="{{ route('vessels.edit', $vessel->id) }}">{{ $vessel->present()->name }}</a></td>
      <td>
        <a href="{{ route('vessels.edit', $vessel->id) }}" class="btn btn-default btn-xs">Edit</a>
      </td>
    </tr>
    @endforeach


  </tbody>
  </table>

  <div class="panel-footer clearfix">
    <div class="pull-right">
      {{ $vessels->appends(Input::except('page'))->links() }}
    </div>
  </div>
</div>
@stop