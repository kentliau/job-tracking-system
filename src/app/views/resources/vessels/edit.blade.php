@extends('layouts.default')

@section('sidebar')
  @include('resources._sidebar')
@stop

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="well">

      {{ Form::model($vessel, ['route' => ['vessels.update', $vessel->id], 'method' => 'put', 'class' => 'form-horizontal']) }}
        <fieldset>

          <legend>{{ trans('jts.editing_vessel') }}</legend>

          @include('resources.vessels._form')

          <div class="form-group">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="pull-right">
                  {{ Form::submit(trans('ui.update'), ['class' => 'btn btn-primary']) }}
                  {{ Form::button(trans('ui.undo_changes'), ['class' => 'btn btn-default', 'type' => 'reset']) }}
                </div>
                <div class="pull-left">
                  <a class="btn btn-danger" href="{{ route('vessels.destroy', $vessel->id ) }}" data-method="delete">{{ trans('ui.delete') }}</a>
                </div>
            </div>
          </div>

        </fieldset>
      {{ Form::close() }}

    </div> <!-- .well -->
  </div> <!-- .col -->
</div> <!-- .row -->

@stop