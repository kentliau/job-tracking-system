<div class="form-group">
  {{ Form::label('name', trans('jts.name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('description', trans('jts.description'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('description', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('description', '<span class="help-block">:message</span>') }}
  </div>
</div>