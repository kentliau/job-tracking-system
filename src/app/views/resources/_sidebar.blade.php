<li class="{{ Request::is('documents') ? 'active' : '' }}">
  <a href="{{ route('documents.index') }}">
  <i class="glyphicon glyphicon-folder-open"></i>
  {{ trans('jts.documents') }}
  </a>
</li>

<li class="{{ Request::is('ports') ? 'active' : '' }}">
  <a href="{{ route('ports.index') }}">
  <i class="glyphicon glyphicon-tower"></i>
  {{ trans('jts.ports') }}
  </a>
</li>

<li class="{{ Request::is('vessels') ? 'active' : '' }}">
  <a href="{{ route('vessels.index') }}">
  <i class="glyphicon glyphicon-flag"></i>
  {{ trans('jts.vessels') }}
  </a>
</li>

