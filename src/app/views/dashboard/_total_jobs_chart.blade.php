@section('scripts')
@parent
<script>
var totalJobsChart = c3.generate({
    bindto: '#totalJobsChart',
    color: { pattern: ['#5cb85c', '#428bca', '#d9534f', '#777' ] },
    data: {
        columns: [
            ['Processing', {{ JTS\Entities\Shipments\Shipment::where('status', '=', 'processing')->count() }}],
            ['Completed', {{ JTS\Entities\Shipments\Shipment::where('status', '=', 'completed')->count() }}],
            ['Uncompleted', {{ JTS\Entities\Shipments\Shipment::where('status', '=', 'uncompleted')->count() }}],
            ['Cancelled', {{ JTS\Entities\Shipments\Shipment::where('status', '=', 'cancelled')->count() }}],
        ],
        type : 'donut'
    },
    donut: {
        title: "Status",
        label: {
          format: function (value) { return value; }
        }
    }
});


</script>
@stop
{{--
<div class="container-fluid">

<div class="row">

<div class="col-xs-3">
  <div class="panel panel-success">
    <div class="panel-heading">
      <h3 class="panel-title">{{ trans('jts.processing') }}</h3>
    </div>
    <div class="panel-body">
      <div class="dashboard-figure text-center">60</div>
    </div>
  </div>
</div>


<div class="col-xs-3">
  <div class="panel panel-primary">
    <div class="panel-heading">
      <h3 class="panel-title">{{ trans('jts.completed') }}</h3>
    </div>
    <div class="panel-body">
      <div class="dashboard-figure text-center">20</div>
    </div>
  </div>
</div>

<div class="col-xs-3">
  <div class="panel panel-danger">
    <div class="panel-heading">
      <h3 class="panel-title">{{ trans('jts.uncompleted') }}</h3>
    </div>
    <div class="panel-body">
      <div class="dashboard-figure text-center">15</div>
    </div>
  </div>
</div>


<div class="col-xs-3">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title">{{ trans('jts.cancelled') }}</h3>
    </div>
    <div class="panel-body">
      <div class="dashboard-figure text-center">5</div>
    </div>
  </div>
</div>


</div>

<div class="row">
<div class="col-xs-12">
<p>Total of 100 jobs.</p>
</div>
</div>

</div>
--}}