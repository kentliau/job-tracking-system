<table class="table table-condensed table-hover">
  <thead>
    <tr>
      <th>{{ trans('ui.messages') }}</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td><a href="">Darius updated Documents of Job EX14-123 10 minutes ago.</a></td>
    </tr>
    <tr>
      <td><a href="">Christina updated Items of Job EX14-123 20 minutes ago.</a></td>
    </tr>
    <tr>
      <td><a href="">Gareth updated Invoices of Job EX14-123 an hour ago.</a></td>
    </tr>
    <tr>
      <td><a href="">Martin updated Costsheets of Job EX14-123 a day ago.</a></td>
    </tr>
    <tr>
      <td><a href="">Tyreese updated Tasks of Job EX14-123 2 days ago.</a></td>
    </tr>
    <tr>
      <td><a href="">Carol updated Job EX14-123 10 days ago.</a></td>
    </tr>

  </tbody>
</table>

<a href="">View all <i class="glyphicon glyphicon-circle-arrow-right"></i></a>