@extends('layouts.default_simple')

@section('breadcrumb')
@stop

@section('content')
{{--
<div class="row">
  <div class="col-md-12">
    <div class="panel panel-default">

      <div class="panel-body">
      @include('dashboard._tabbed_view')
      </div>
    </div>
  </div>
</div>
--}}

<div class="row">

  <div class="col-md-6">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i>{{ trans('jts.total_company_jobs') }} (Total: {{ JTS\Entities\Shipments\Shipment::count() }})</h3>
      </div>
      <div class="panel-body">
        <div id="totalJobsChart">
          @include('dashboard._total_jobs_chart')
        </div>
      </div>
    </div>

  </div>

  <div class="col-md-6">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-stats"></i>{{ trans('jts.total_individual_jobs') }} (Total: {{ JTS\Entities\Users\User::find(Sentry::getUser()->id)->shipments()->count() }})</h3>
      </div>
      <div class="panel-body">
        <div id="totalIndividualChart">
          @include('dashboard._total_individual_chart')
        </div>
      </div>
    </div>

  </div>

</div>


<div class="row">
{{--
  <div class="col-md-6">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-bell"></i>{{ trans('jts.job_notifications') }}</h3>
      </div>
      <div class="panel-body">
        <div id="notificationsTable">
          @include('dashboard._notifications_table')
        </div>
      </div>
    </div>

  </div>
--}}
  <div class="col-md-6">

    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="glyphicon glyphicon-th-list"></i>{{ trans('jts.job_handled_by_you') }}</h3>
      </div>
      <div class="panel-body">
        <div id="individualJobsTable">
          @include('dashboard._individual_jobs_table')
        </div>
      </div>
    </div>
  </div>

</div>

@stop

@section('scripts')
@parent
  @if(Input::has('demo'))
  <script src="{{ asset('assets/js/demo.js') }}"></script>
  @endif
@stop