<table class="table table-condensed table-hover">
  <thead>
    <tr>
      <th>{{ trans('jts.shipment_jobs') }}</th>
      <th>Last Update</th>
    </tr>
  </thead>
  <tbody>
    @foreach( JTS\Entities\Users\User::find(Sentry::getUser()->id)->shipments as $shipment)
    <tr>
      <td><a href="{{ route('shipments.edit', $shipment->id) }}">{{ $shipment->present()->fileNumber }}</a></td>
      <td>{{ $shipment->updated_at->format(Config::get('app.date_format')) }}</td>
    </tr>
    @endforeach

  </tbody>
</table>

<a href="{{ route('shipments.index') }}">View all <i class="glyphicon glyphicon-circle-arrow-right"></i></a>