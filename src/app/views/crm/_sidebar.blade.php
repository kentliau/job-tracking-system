<li class="{{ Request::is('customers') ? 'active' : '' }}">
  <a href="{{ route('customers.index') }}">
  <i class="glyphicon glyphicon-user"></i>
  {{ trans('jts.customers') }}
  </a>
</li>

