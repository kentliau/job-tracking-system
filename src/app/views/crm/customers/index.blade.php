@extends('layouts.default')

@section('sidebar')
  @include('crm._sidebar')
@stop

@section('breadcrumb')
@stop

@section('content')

<div class="row">
  <div class="col-md-12">

    <div class="panel panel-default">
      <div class="panel-heading clearfix">
        <strong class="panel-title">@lang('jts.customers')</strong>
        <div class="pull-right">
          <a class="btn btn-primary btn-sm" href="{{ route('customers.index') }}?export=csv"><i class="glyphicon glyphicon-export"></i>@lang('jts.export_to_csv_card')</a>
          <a class="btn btn-primary btn-sm" href="{{ route('customers.create') }}"><i class="glyphicon glyphicon-plus"></i>@lang('jts.new_customer')</a>
        </div>
      </div>
      <table class="table table-striped table-bordered table-hover table-condensed">
        <thead>
          <tr>
            <th>@lang('jts.company_name')</th>
            <th>@lang('jts.contact_name')</th>
            <th>@lang('jts.phone_number')</th>
            <th>@lang('jts.fax_number')</th>
            <th>@lang('jts.email')</th>
            <th>@lang('jts.address')</th>
            <th>@lang('jts.description')</th>
          </tr>
        </thead>
        <tbody>
            @foreach($customers as $customer)
              <tr>
                <td><a href="{{ route('customers.edit', $customer->id) }}">{{ $customer->present()->name }}</a></td>
                <td>{{ $customer->contact_name }}</td>
                <td>{{ $customer->phone_number }}</td>
                <td>{{ $customer->fax_number }}</td>
                <td>{{ $customer->email }}</td>
                <td>{{ $customer->address }}</td>
                <td>{{ $customer->present()->description }}</td>
              </tr>
            @endforeach
        </tbody>
      </table>

      <div class="panel-footer clearfix">
        <div class="pull-right">
          {{ $customers->appends(Input::except('page'))->links() }}
        </div>
      </div>
    </div>

  </div>
</div>

@stop