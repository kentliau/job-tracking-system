<div class="form-group">
  {{ Form::label('name', trans('jts.name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('contact_name', trans('jts.contact_name'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('contact_name', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('contact_name', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('description', trans('jts.description'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('description', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('description', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('phone_number', trans('jts.phone_number'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('phone_number', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('phone_number', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('fax_number', trans('jts.fax_number'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('fax_number', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('fax_number', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('email', trans('ui.email'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::email('email', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('email', '<span class="help-block">:message</span>') }}
  </div>
</div>

<div class="form-group">
  {{ Form::label('address', trans('jts.address'), ['class' => 'col-md-2  control-label'] ) }}
  <div class="col-md-10">
    {{ Form::text('address', null, ['class' => 'form-control'] ) }}
    {{ $errors->first('address', '<span class="help-block">:message</span>') }}
  </div>
</div>
