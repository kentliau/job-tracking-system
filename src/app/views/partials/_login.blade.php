<div class="row">
    <div class="col-sm-offset-1 col-md-offset-4 col-lg-offset-4 col-xs-12 col-sm-10 col-md-4 col-lg-4">

    <div class="panel panel-default">
      <div class="panel-heading">
        <div class="text-center panel-title"><i class="glyphicon glyphicon-lock"></i>Authorized Personnel Only</div>
      </div>
      <div class="panel-body">

            {{ Form::open(['route' => 'sessions.store', 'method' => 'post', 'class' => '']) }}
              <div class="form-group">
                <label for="email" class="control-label">Email</label>
                  {{ Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'john@appleseed.com']) }}
              </div>
              <div class="form-group">
                <label for="password" class="control-label">Password</label>
                  {{ Form::password('password', ['class' => 'form-control', 'placeholder' => 'Password'] ) }}
              </div>
              <div class="form-group">
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="remember_me"> Remember me
                    </label>
                  </div>
              </div>
              <div class="form-group">
                <button type="submit" class="btn btn-primary btn-block">Sign in</button>
              </div>
            {{ Form::close() }}

      </div>
    </div>

  </div>
</div>