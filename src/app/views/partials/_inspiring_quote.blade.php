<div class="callout callout-danger" role="alert">
<h4>Landing Alert</h4>
We have detached from Mothership, and now preparing for landing. Philae detached from Rosetta on 12 November 2014 at 08:35 UTC, landing seven hours later at 15:35. The <strong><a href="http://msdn.microsoft.com/en-us/library/ff921069(v=pandp.20).aspx">awesomeness of modularity</a></strong>.
</div>
