@section('breadcrumb')
  @if ( count( Request::segments() ) >= 1 )
    <ol class="breadcrumb">
    @foreach( Request::segments() as $index => $segment )

      @if (! in_array($segment, ['create', 'edit']))

        @if ( $index == 1 and Request::segment(1) === 'shipments' and is_numeric( Request::segment(2) ) and isset($fileNumber))
          <li><a href="{{ url( implode( '/', array_slice(Request::segments(), 0, $index+1) ) ) }}" class="text-capitalize">{{ $fileNumber }}</a></li>
        @else
          @if ( ($index == 1 or $index == 2) and Request::segment(1) === 'shipments' and ! is_numeric( Request::segment(2) ) )
            @if ( $index == 1 )
              <li>
                <a href="{{ url( implode( '/', array_slice(Request::segments(), 0, $index+1) ) ) }}/{{ Request::segment(3) }}" class="text-capitalize">{{ $segment }}/{{ Request::segment(3) }}</a>
              </li>
            @endif
          @else
            <li>
              <a href="{{ url( implode( '/', array_slice(Request::segments(), 0, $index+1) ) ) }}" class="text-capitalize">{{ $segment }}</a>
            </li>
          @endif
        @endif

      @endif

    @endforeach
    </ol>
  @endif
@show