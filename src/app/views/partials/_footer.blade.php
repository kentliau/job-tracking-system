<footer>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <hr/>
      </div>
    </div>
  </div>
  <div class="container">

    <div class="row">

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
         <ul>
           <li><a href="{{ url('endpoints') }}">{{ trans('ui.endpoints') }}</a></li>
           <li><a href="{{ url('help') }}">{{ trans('ui.help') }}</a></li>
         </ul>
      </div> <!-- .col -->

      <div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
         <ul>
           <li><a href="{{ url('policy') }}">{{ trans('ui.policy') }}</a></li>
           <li><a href="{{ url('cookies') }}">{{ trans('ui.cookies') }}</a></li>
           <li><a href="{{ url('legal') }}">{{ trans('ui.legal') }}</a></li>
           <li><a href="{{ url('copyright') }}">{{ trans('ui.copyright') }}</a></li>
         </ul>
      </div> <!-- .col -->

    </div>
  </div>
</footer>
