@if ($message = Session::get('success'))
<div class="alert alert-success alert-block">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>{{ trans('ui.success') }}</h4>
  {{ $message }}
</div>
@endif

@if ($message = Session::get('info'))
<div class="alert alert-info alert-block">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>{{ trans('ui.info') }}</h4>
  {{ $message }}
</div>
@endif
