@if($errors->any())
<div class="panel panel-danger">
  <div class="panel-heading">
    <h3 class="panel-title">{{ trans('str/error.we_encountered_the_following_errors') }}
    </h3>
  </div>
  <div class="panel-body">
  <ul>
    @foreach($errors->all() as $message)
      <li>{{ $message }}</li>
    @endforeach
  </ul>
  </div>
</div>
@endif

@if ($message = Session::get('error'))
<div class="alert alert-warning alert-dismissable">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>{{ trans('ui.errors') }}</h4>
  {{ $message }}
</div>
@endif

@if ($message = Session::get('warning'))
<div class="alert alert-warning alert-block">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  <h4>{{ trans('ui.warning') }}</h4>
  {{ $message }}
</div>
@endif
