<nav class="navbar navbar-inverse">
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-inverse-collapse">
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" title="{{ trans('ui.home') }}" href="/">TT</a>
  </div>
  <div class="navbar-collapse collapse navbar-inverse-collapse">
    <ul class="nav navbar-nav">
      <li><a title="{{ trans('jts.dashboard') }}" href="{{ url('dashboard') }}">{{ trans('jts.dashboard')  }}</a></li>
      <li><a title="{{ trans('jts.human_resources_management') }}" href="{{ url('hrm') }}">{{ trans('jts.human_resources_management_abbr') }}</a></li>
      <li><a title="{{ trans('jts.customer_relationship_management') }}" href="{{ url('crm') }}">{{ trans('jts.customer_relationship_management_abbr') }}</a></li>
      <li><a title="{{ trans('jts.resources_management') }}" href="{{ url('rm') }}">{{ trans('jts.resources_management_abbr') }}</a></li>
      <li><a title="{{ trans('jts.shipment_jobs') }}" href="{{ route('shipments.index')  }}">{{ trans('jts.shipment_jobs') }}</a></li>
    </ul>

    <ul class="nav navbar-nav navbar-right">
      <li>
        <form class="navbar-form navbar-left" action="{{ url('search') }}">
          <div class="form-group">
             <div class=" col-lg-12">
                <input type="text" class="form-control" name="q" placeholder="e.g. EX-14/001">
            </div>
          </div>
        </form>
      </li>

      @if(Sentry::check())
      <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Hi, {{ Sentry::getUser()->present()->identifier }} <b class="caret"></b></a>
        <ul class="dropdown-menu">
          <li class="disabled"><a href="{{ url('preferences') }}">{{ trans('ui.preferences') }}</a></li>
          <li><a href="{{ url('help') }}">{{ trans('ui.help') }}</a></li>
          <li class="divider"></li>
          <li><a href="{{ route('logout') }}">{{ trans('ui.logout') }}</a></li>
        </ul>
      </li>
      @else
      <ul class="nav navbar-nav">
        <li><a title="{{ trans('jts.dashboard') }}" href="{{ url('dashboard') }}">{{ trans('ui.not_login')  }}</a></li>
      </ul>
      @endif
    </ul>
  </div>
</nav>
