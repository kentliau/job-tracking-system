<?php

return array(

  // Here only overiding the port and the host address
  // this is to allow us to run `artisan migrate` at
  // host machine (the computer you use to do coding)
  // instead of ssh into the VM to run `artisan migrate`
  // Note: remember to add your host machine name to the start.php
  // and make it development env.
  'connections' => array(

    'mysql' => array(
      'driver'    => 'mysql',
      'host'      => '127.0.0.1',
      'port'      => '33060',
      'database'  => 'homestead',
      'username'  => 'homestead',
      'password'  => 'secret',
      'charset'   => 'utf8',
      'collation' => 'utf8_unicode_ci',
      'prefix'    => '',
    ),

  ),

);
