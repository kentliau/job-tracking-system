<?php

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before( function ($request)
{
  //
} );


App::after( function ($request, $response)
{
  //
} );

App::missing( function ($exception)
{
  return Response::view( 'errors.404', [ ], 404 );
} );

// App::error(function($exception, $code)
// {
//   switch ($code)
//   {
//     case 403:
//       return Redirect::back()->with('error', '403 Permission Problem');

//     case 404:
//       return Redirect::to('/');

//     case 500:
//       return Redirect::back()->with('error', '500 server boom! <br>' . '<pre>' , $exception . '</pre>');

//     default:
//       return Redirect::back()->with('error', $code);
//   }
// });

App::error( function (\Illuminate\Database\Eloquent\ModelNotFoundException $exception)
{
  return Response::view( 'errors.404', [ ], 404 );
} );

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter( 'auth', function ()
{
  if ( ! Sentry::check() )
  {
    return Redirect::guest( '/' );
  }
} );

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter( 'guest', function ()
{
  if ( ! Sentry::check() )
  {
    return Redirect::to( '/' );
  }
} );

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter( 'csrf', function ($route, $request)
{
  $token = Request::ajax() ? Request::header('X-CSRF-Token') : Input::get('_token');

  if ( $request->getMethod() === 'POST' )
  {
    if ( Session::token() !== $token )
    {
      throw new Illuminate\Session\TokenMismatchException;
    }
  }
} );


Route::filter( 'isAdmin', function ()
{
  // return true if admin
  // false if NOT admin
} );


Route::filter( 'expiry', function ($route, $request, $value)
{
  // $now = \Carbon\Carbon::now();
  // $expiredAt = $value::find($request->segment(2))->expired_at;

  // $seconds = $now->diffInSeconds($expiredAt, false);

  // if ($seconds <= 0)
  // {
  // 	// return Redirect::route("shipments.show", $request->segment(2))->withErrors('test');
  // }
//
} );